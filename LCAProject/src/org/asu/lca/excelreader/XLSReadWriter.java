/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package org.asu.lca.excelreader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.TableModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.asu.lca.sharedobject.SharedDataObject;

/**
 *
 * The <code>XLSReadWriter</code> is the class for processing ".xls" format
 * files
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi (nthottem@asu.edu)
 * @version Feb 9, 2014
 * 
 * Update: mison updated sdo constructor calls after loading data
 */
public class XLSReadWriter implements ExcelReadWriter {
    private static final Integer LCA_HEADER_LENGTH = 2;
    private static final Integer ALTERNATIVES_BEGIN_INDEX = 2;
    private static final Integer ALTERNATIVES_LENGTH = 3;
    public XLSReadWriter() {
        
    }
    /**
     * reads Excel file with LCA header and created Bundle of the data 
     * @param f
     * @return SharedDataObject- bundle containing the processed data
     * @throws org.asu.lca.excelreader.IncorrectFileFormatException
     */
    @Override
    public SharedDataObject processLCAFile(FileInputStream f) 
            throws IncorrectFileFormatException {
        ArrayList<String> alternatives = new ArrayList<>();
        ArrayList<String> columnType = new ArrayList<>();
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> rowValues = new ArrayList<>();
        try {
            HSSFWorkbook wb = new HSSFWorkbook(f);
            wb.setMissingCellPolicy(Row.RETURN_BLANK_AS_NULL);
            int currentSheet = wb.getActiveSheetIndex();
            HSSFSheet sh = wb.getSheetAt(currentSheet);
            int rows = sh.getPhysicalNumberOfRows();
            if(rows == 0 || rows < 3)
                throw new IncorrectFileFormatException("Empty File");
            System.out.println("Number of Rows: "+ rows);
            //first row contains alternatives names
            //process header and limit cell size
            HSSFRow row = sh.getRow(2);
            int colLen = row.getPhysicalNumberOfCells();
            System.out.println("col len: "+ colLen);
            if(colLen < 2+2*ALTERNATIVES_LENGTH) {
                String msg = "insufficient data";
                System.out.println(msg);
                throw new IncorrectFileFormatException(msg);
            }
            int activeColLen = 0;
            for(int col = 0; col < colLen; col++) {
                if(row.getCell(col) == null) {
                    break;
                } else {
                    activeColLen++;
                }
            }
            if(activeColLen < colLen) {
               colLen = activeColLen;
            }
            if(colLen < (2 + 2*ALTERNATIVES_LENGTH) || 
                    (colLen-2)%ALTERNATIVES_LENGTH != 0) {
                String msg = "incorrect data. Pointer: Look at first data row";
                System.out.println(msg);
                throw new IncorrectFileFormatException(msg);
            }
            System.out.println("active column length :" + colLen);
            for(int i = 0; i < rows; i++) {
                //System.out.println("Current row "+ (i));
                row = sh.getRow(i);
                if(row == null) {
                    String msg = "row("+(i+1)+") is empty";
                    throw new IncorrectFileFormatException(msg);
                }
                //int cells = row.getPhysicalNumberOfCells();
                //System.out.println("Number of cols: " + cells);
                HSSFCell cell = row.getCell(0);
                if(cell == null && i >= 1) {
                    System.out.print("cell("+i+",0) is empty. Restricting the rowLength");
                    System.out.print("active row length: "+ (i-1));
                    break;
                }
                //clear column values for next processing
                rowValues.clear();
                for(int c = 0; c < colLen; c++) {
                    //System.out.print("Current column "+ (c));
                    cell = row.getCell(c);

                    if(cell == null) {
                        if(i >= 1) {
                            String msg = "cell("+(i+1)+","+(c+1)+") is empty";
                            System.out.println(msg);
                            throw new IncorrectFileFormatException(msg);
                        }
                        //System.out.println(" is empty");
                        rowValues.add(null);
                        continue;
                    }
                    String value = null;

                    switch (cell.getCellType()) {

                            case HSSFCell.CELL_TYPE_FORMULA:
                                    value = cell.getCellFormula();
                                    break;

                            case HSSFCell.CELL_TYPE_NUMERIC:
                                    value = String.valueOf(cell.getNumericCellValue());
                                    break;

                            case HSSFCell.CELL_TYPE_STRING:
                                    value = cell.getStringCellValue();
                                    break;

                            default:
                                
                    }
                    //System.out.println("CELL col=" + cell.getColumnIndex() + " VALUE="
                      //              + value+" CELL_TYPE: "+ cell.getCellType());
                    if(i >= LCA_HEADER_LENGTH && 
                            (value == null || value.length() == 0))
                        throw new IncorrectFileFormatException("cell("+(i+1)+","+(c+1)+") is empty");
                    if(i >= LCA_HEADER_LENGTH && c >= 2 && !(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC || cell.getCellType() == HSSFCell.CELL_TYPE_FORMULA)) {
                        throw new IncorrectFileFormatException("cell("+(i+1)+","+(c+1)+") contains non-numeric value");
                    }
                  rowValues.add(value);                    
                }
                
                //add column data
                //process alternatives
                if(i == 0) {
                    //From column index 2, alternatives names will be present
                    System.out.println("size: "+ rowValues.size());
                    String msg = "Error: Incorrect file format"
                                + " please enter first row alternatives properly";
                    if(rowValues.size() < ALTERNATIVES_BEGIN_INDEX+1) {
                        System.out.println(msg);
                        throw new IncorrectFileFormatException(msg); 
                    }
                    //skip by 3 steps
                    for(int colIndex = 2; colIndex < (rowValues.size()-ALTERNATIVES_LENGTH+1); 
                            colIndex += ALTERNATIVES_LENGTH) {
                        // any of consecutive 3 indices can contain valid value
                        String aName = rowValues.get(colIndex);
                        if(aName == null) {
                            aName = rowValues.get(colIndex + 1);
                            if(aName == null) {
                                aName = rowValues.get(colIndex + 2);
                            }
                            if(aName == null) {
                                System.out.println(msg);
                                throw new IncorrectFileFormatException(msg);
                            }
                        }
                        alternatives.add(aName);
                    }
                } else if(i == 1) {
                    //process Alternatives
                    columnType.addAll(rowValues);
                } else {
                    data.add(new ArrayList(rowValues));
                }
            }
            //Bundle the object
            //Modified by mison to correspond with alternative and indicator objects
            SharedDataObject sdo = new SharedDataObject(alternatives, columnType, data);
            //sdo.setAlternatives(alternatives);
            //sdo.setColumnTypes(columnType);
            //sdo.setData(data);
            return sdo;

        } catch (FileNotFoundException e) {
                System.out.println("File not found");
        } catch (IOException e) {
                System.out.println("IO Exception");
        }
        return null;
    }

    /**
     * reads Single line header excel file and creates bundle of data
     * @param f
     * @return  SharedDataObject- bundle containing the processed data
     * @throws org.asu.lca.excelreader.IncorrectFileFormatException
     */
    @Override
    public SharedDataObject processFile(FileInputStream f) 
            throws IncorrectFileFormatException {
        ArrayList<String> columnType = new ArrayList<>();
        ArrayList<ArrayList<String>> data = new ArrayList<>();
        ArrayList<String> rowValues = new ArrayList<>();
        try {
            HSSFWorkbook wb = new HSSFWorkbook(f);
            int currentSheet = wb.getActiveSheetIndex();
            HSSFSheet sh = wb.getSheetAt(currentSheet);
            int rows = sh.getPhysicalNumberOfRows();
            System.out.println("Number of Rows: "+ rows);
            //first row contains alternatives names
            for(int i = 0; i < rows; i++) {
                //System.out.println("Current row "+ (i));
                HSSFRow row = sh.getRow(i);
                if(row == null) {
                    String msg = "row("+(i+1)+") is empty";
                    throw new IncorrectFileFormatException(msg);
                }
                int cells = row.getPhysicalNumberOfCells();
                //clear column values for next processing
                rowValues.clear();
                for(int c = 0; c < cells; c++) {
                    //System.out.print("Current column "+ (c));
                    HSSFCell cell = row.getCell(c);

                    if(cell == null) {
                        if(i >= 1) {
                            String msg = "cell("+(i+1)+","+(c+1)+") is empty";
                            System.out.println(msg);
                            throw new IncorrectFileFormatException(msg);
                        }
                        //System.out.println(" is empty");
                        rowValues.add(null);
                        continue;
                    }
                    String value = null;

                    switch (cell.getCellType()) {

                            case HSSFCell.CELL_TYPE_FORMULA:
                                    value = cell.getCellFormula();
                                    break;

                            case HSSFCell.CELL_TYPE_NUMERIC:
                                    value = String.valueOf(cell.getNumericCellValue());
                                    break;

                            case HSSFCell.CELL_TYPE_STRING:
                                    value = cell.getStringCellValue();
                                    break;

                            default:

                    }
                    if(i >= 1 && 
                            (value == null || value.length() == 0))
                        throw new IncorrectFileFormatException("cell("+(i+1)+","+(c+1)+") is empty");
                    rowValues.add(value);
                    //System.out.println("CELL col=" + cell.getColumnIndex() + " VALUE="
                      //              + value+" CELL TYPE="+cell.getCellType());

                }

                //add column data
                if(i == 0){
                   //process Alternatives
                   columnType.addAll(rowValues);
                } else {
                   data.add(new ArrayList(rowValues));
                }
            }

            //Bundle the object
            // Edited by mison to coordinate with alternative and indicator objects
            SharedDataObject sdo = new SharedDataObject(columnType, data);
            //sdo.setAlternatives(null);
            //sdo.setColumnTypes(columnType);
            //sdo.setData(data);
            return sdo;

        } catch (FileNotFoundException e) {
                System.out.println("File not found");
        } catch (IOException e) {
                System.out.println("IO Exception");
        }
        return null; 
        
    }

    /**
     * 
     * Loads data from table and save as ".xls"
     * @param file
     * @param t 
     */
    @Override
    public void writeToFile(File file, TableModel t) {
        Integer sheetId = 1;
        try {
            Workbook wb = new HSSFWorkbook();;
            Sheet sheet = wb.createSheet("Sheet"+sheetId.toString());
            int rowCount = t.getRowCount();
            int columnCount = t.getColumnCount();
            Row row = sheet.createRow((short)0);
            for(int i = 0; i < columnCount; i++) {
                row.createCell(i).setCellValue(t.getColumnName(i));
            }
            for(int i = 0; i < rowCount; i++) {
                row = sheet.createRow((short)(i+1));
                for(int j = 0; j < columnCount; j++) {
                    row.createCell(j).setCellValue(t.getValueAt(i,j).toString());
                }
            }
            FileOutputStream  fOutput = new FileOutputStream(file);
            wb.write(fOutput);
            fOutput.close();
        } catch (IOException ex) {
            Logger.getLogger(XLSReadWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * Loads data from table and save as ".xls"
     * @param file
     * @param tModels
     */
    @Override
    public void writeToFile(File file, ArrayList<TableModel> tModels) {
        try {
            FileOutputStream  fOutput = new FileOutputStream(file);
            HSSFWorkbook wb = new HSSFWorkbook();
            for(int tCount = 0; tCount < tModels.size(); tCount++) {
                HSSFSheet sheet = wb.createSheet("Sheet"+tCount);
                int rowCount = tModels.get(tCount).getRowCount();
                int columnCount = tModels.get(tCount).getColumnCount();
                Row row = sheet.createRow((short)0);
                for(int i = 0; i < columnCount; i++) {
                    row.createCell(i).setCellValue(tModels.get(tCount).getColumnName(i));
                }
                for(int i = 0; i < rowCount; i++) {
                    row = sheet.createRow((short)(i+1));
                    for(int j = 0; j < columnCount; j++) {
                        row.createCell(j).setCellValue(tModels.get(tCount).getValueAt(i,j).toString());
                    }
                }
            }
            wb.write(fOutput);
            fOutput.close();
        } catch (IOException ex) {
            Logger.getLogger(XLSReadWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
