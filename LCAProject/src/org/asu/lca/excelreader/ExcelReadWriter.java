/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package org.asu.lca.excelreader;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import javax.swing.table.TableModel;
import org.asu.lca.sharedobject.SharedDataObject;

/**
 *
 * The <code>ExcelReadWriter</code> is the interface for different
 * Excel reader types
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi (nthottem@asu.edu)
 * @version Feb 9, 2014
 */
public interface ExcelReadWriter {
    SharedDataObject processFile(FileInputStream f) throws IncorrectFileFormatException;
    SharedDataObject processLCAFile(FileInputStream f) throws IncorrectFileFormatException;

    public void writeToFile(File file, TableModel t);
    public void writeToFile(File file, ArrayList<TableModel> tModels);
}
