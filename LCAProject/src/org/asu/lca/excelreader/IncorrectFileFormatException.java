/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.asu.lca.excelreader;

/**
 *
 * @author Nagarjuna
 */
public class IncorrectFileFormatException extends Exception {
    private final String msg;
    public IncorrectFileFormatException(String msg) {
        this.msg = msg;
    }
    
    @Override
    public String getMessage() {
        return msg;
    }
}
