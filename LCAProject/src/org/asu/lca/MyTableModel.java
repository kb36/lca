/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca;

import javax.swing.table.AbstractTableModel;

/**
 *
 * The <code>MyTableModel</code> is the class for populating processed excel
 * data to Table
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi (nthottem@asu.edu)
 * @version Feb 9, 2014
 */
  public  class MyTableModel extends AbstractTableModel {  
        String[] columnNames;  
        Object[][] tableData;  
        public MyTableModel(Object[][] o, String[] c){  
                tableData = o;  
                columnNames = c;  
        }  
        @Override
        public int getColumnCount() {  
            return columnNames.length;  
        }  
        @Override
        public int getRowCount() {  
            return tableData.length;  
        }  
        @Override
        public String getColumnName(int col) {  
            return columnNames[col];  
        }  
        @Override
        public Object getValueAt(int row, int col) {  
            return tableData[row][col];  
        }  
        @Override
        public Class getColumnClass(int c) {  
            return getValueAt(0, c).getClass();  
        }  
        @Override
        public boolean isCellEditable(int row, int col) {  
                return false;  
        }  
        @Override
        public void setValueAt(Object value, int row, int col) {  
        }  
    } 
