/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca;

import java.awt.event.ActionListener;

import java.io.File;

import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import javax.swing.JButton;


import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * This is the view providing all the GUI for interaction
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Apr 6, 2014
 * 
 * BUTTONS TO SAVE/LOAD RANKINGS.
 * RIGHT SIDE INTERACTION
 */
public class LCAview extends JFrame {
  JMenuItem inputFileFormatItem;
  JMenuItem aboutItem;
  JFrame aboutFrame;
  
  JFileChooser fileChooser;
  
  JButton loadDataButton;
  JButton saveRankButton;
  
  JPanel dataPanel;
  JTable dataTable;
  JScrollPane dataTableScroll;
  
  JPanel rankPanel;
  JTable minTable;
  JScrollPane minTableScroll;
  JTable meanTable;
  JScrollPane meanTableScroll;
  
  JPanel optPanel;
  JComboBox areaTypeBox;
  JSpinner numIndicatorSpin;
  JSpinner numRunSpin;
  JComboBox distTypeBox;
  JButton runAnalysisButton;
  
  JScrollPane priorityScroll;
  JSlider[] prioritySliders;
  //JButton priorityButton;
  
  JPanel resPanel;
  
  JLabel compareLabel;
  JButton compareButton;
  
  JTable altRankTable;
  JScrollPane altRankTableScroll;
  JButton altRankButton;
  
  JComboBox alternativesBox;
  JComboBox indicatorsBox;
  JLabel contLabel;
  JButton contButton; // Not needed if using JFreeChart
  
  JLabel weightLabel;
  JButton weightButton; 
 
  Dimension screenSize;
  
  int windowWidth;
  int windowHeight;
  
  private static final int MEAN_AREA = LCAmodel.MEAN_RANK;
  private static final int MIN_AREA = LCAmodel.MIN_RANK;
  private static final int MIN_IND = 0;
  private static final int MEAN_IND = 1;
  
  private static final int PDF = LCAmodel.PDF;
  private static final int CDF = LCAmodel.CDF;
  private static final int PDF_IND = 0;
  private static final int CDF_IND = 1;
  
  private static final String[] DIST_PLOT_TITLES = {"Probability Distribution", "Cumulative Distribution"};
  
  private static final String[] AREA_OPTS = {"Min Areas", "Mean Areas"};
  private static final String[] DIST_OPTS = {"PDF", "CDF"};
  
  public LCAview() {
    screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    
    windowWidth = (3*screenSize.width)/4;
    windowHeight = (3*screenSize.height)/4;
    
    JMenuBar menuBar = initMenuBar();
    JPanel lcaPanel = initDataPanel();
    
    setJMenuBar(menuBar);
    
    this.add(lcaPanel);
    this.setSize(windowWidth, windowHeight);
    this.setLocation( (screenSize.width-this.getWidth())/4,
                      (screenSize.height-this.getHeight())/4);
    
    this.setVisible(true);
    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    this.setTitle("LCA-SMAA");
    this.setResizable(false);
  }
  
  public void addLoadDataListener(ActionListener loadDataListener) {
    loadDataButton.addActionListener(loadDataListener);
  }
  
  public void addInputFileFormatListener(ActionListener fileFormatListener) {
    inputFileFormatItem.addActionListener(fileFormatListener);
  }
  
  public void addAboutListener(ActionListener aboutListener) {
    aboutItem.addActionListener(aboutListener);
  }
  
  public void addSaveRankListener(ActionListener saveRankListener) {
    saveRankButton.addActionListener(saveRankListener);
  }
  
  public void addRunListener(ActionListener runListener) {
    runAnalysisButton.addActionListener(runListener);
    //priorityButton.addActionListener(runListener);
  }
  
  public void addIndSelectListener(ActionListener aListener, ChangeListener cListener) {
    numIndicatorSpin.addChangeListener(cListener);
    areaTypeBox.addActionListener(aListener);
  }
  
  public void addContributionListener(ActionListener contListener) {
    contButton.addActionListener(contListener);
  }
  
  public void addComparisonListener(ActionListener compListener) {
    compareButton.addActionListener(compListener);
  }
  
  public void addWeightPlotListener(ActionListener wListener) {
    weightButton.addActionListener(wListener);
  }
  
  public void addRankPlotListener(ActionListener rankListener) {
    altRankButton.addActionListener(rankListener);
  }
  
  public void showMessage(String s1, String s2, Integer type) {
    JOptionPane.showMessageDialog(this, s1, s2, type);
  }
  
  public File selectDataFile() {
    int returnVal = fileChooser.showOpenDialog(this);
    
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      return fileChooser.getSelectedFile();
    }
    return null;
    
  }
  
  public File selectSaveRankFile(String fileName) {
    fileChooser.setSelectedFile(new File(fileName));
    int returnVal = fileChooser.showSaveDialog(this);
    
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      return fileChooser.getSelectedFile();
    }
    return null;
    
  }
  
  public void setDataTable(String[] columnType, Object[][] data) {
    MyTableModel model = new MyTableModel(data, columnType);
    dataTable.setModel(model);
    //Dimension tableDim = setTableDim(screenSize.width*2/3, screenSize.height/2, dataTable);
    dataTableScroll.setViewportView(dataTable);
    dataTableScroll.setVisible(true);
    //dataTableScroll.setMaximumSize(tableDim);
    //dataTableScroll.setMinimumSize(tableDim);
    loadDataButton.setText("Load New Data...");
    this.pack();
    this.setLocation(0,0);
  }
  
  public void setRankingTable(Integer tableType, String[] columnType, Object[][] data) {
    MyTableModel model = new MyTableModel(data, columnType);
    setIndicatorBound(data.length);
    
    //Dimension tableDim;
    if (tableType == MIN_AREA) {
      minTable.setModel(model);
      minTableScroll.setViewportView(minTable);
      //tableDim = setTableDim(screenSize.width/4, screenSize.height/3, minTable);
      //minTableScroll.setMaximumSize(tableDim);
      //minTableScroll.setMinimumSize(tableDim);
    }
    else if (tableType == MEAN_AREA) {
      meanTable.setModel(model);
      meanTableScroll.setViewportView(meanTable);
      //tableDim = setTableDim(screenSize.width/4, screenSize.height/3, meanTable);
      //meanTableScroll.setMaximumSize(tableDim);
      //meanTableScroll.setMinimumSize(tableDim);
    }
    rankPanel.setVisible(true);
    this.pack();
  }
  
  public void setAltRankTable(String[] columnType, Object[][] data) {
    MyTableModel model = new MyTableModel(data, columnType);
    altRankTable.setModel(model);
    DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
    rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
    for (int i = 1; i < altRankTable.getModel().getColumnCount(); i++)
      altRankTable.getColumnModel().getColumn(i).setCellRenderer(rightRenderer);
    Dimension tableDim = setTableDim(screenSize.width/2, screenSize.height/2, altRankTable);
    altRankTableScroll.setViewportView(altRankTable);
    resPanel.setVisible(true);
    altRankTableScroll.setMaximumSize(tableDim);
    altRankTableScroll.setMinimumSize(tableDim);

    // Show rankings button!
    this.pack();
  }
  
  public void setContributionOptions(String[] altNames, String[] indNames) {
    String[] altOpts = new String[altNames.length+1];
    altOpts[0] = "All";
    System.arraycopy(altNames, 0, altOpts, 1, altNames.length);
    String[] indOpts = new String[indNames.length+1];
    indOpts[0] = "All";
    System.arraycopy(indNames, 0, indOpts, 1, indNames.length);
    
    alternativesBox.setModel(new DefaultComboBoxModel(altOpts));
    indicatorsBox.setModel(new DefaultComboBoxModel(indOpts));
  }
  
  public void plotAltRankings(double[][] data, String[] labels) {
    plot3DRankings(rankToFile(data), labels);
  }
  
  public void plotContribution(double[][] data, String[] labels, boolean newWindow) {
    int distInd = CDF_IND;
    if (getDistType()==PDF)
      distInd = PDF_IND;
    File labelPic = plotData(data,labels, 1, DIST_PLOT_TITLES[distInd] + " Contributions", newWindow);
    if (labelPic != null) {
      /*
      while (labelPic.length() < 10000) {
        try {
          Thread.sleep(100);
        } catch (InterruptedException ex) {
          Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
        }
      } */
      contLabel.setIcon(new ImageIcon(labelPic.toString()));
      contLabel.setText("");
    }
  }
  
  public void plotComparison(double[][] data, String[] labels, boolean newWindow) {
    int distInd = CDF_IND;
    if (getDistType()==PDF)
      distInd = PDF_IND;
    File labelPic = plotData(data, labels, 1, DIST_PLOT_TITLES[distInd] + " Comparison", newWindow);
    if (labelPic != null) {
      /*
      while (labelPic.length() < 10000) {
        try {
          Thread.sleep(100);
        } catch (InterruptedException ex) {
          Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
        }
      } */
      compareLabel.setIcon(new ImageIcon(labelPic.toString()));
      compareLabel.setText("");
    }
      
  }
  
  public void plotWeights(double[][] data, String[] labels, boolean newWindow) {
    int distInd = CDF_IND;
    if (getDistType()==PDF)
      distInd = PDF_IND;
    File labelPic = plotData(data, labels, 100, DIST_PLOT_TITLES[distInd] + " Weights", newWindow);
    if (labelPic != null) {
      /*
      while (labelPic.length() < 10000) {
        try {
          Thread.sleep(100);
        } catch (InterruptedException ex) {
          Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
        }
      }*/
      weightLabel.setIcon(new ImageIcon(labelPic.toString()));
      weightLabel.setText("");
    }
  }
  

  
  public void showSettings() {
      optPanel.setVisible(true);
      
      this.pack();
  }
  
  public void updatePriorities(String[] indNames, ChangeListener cListener) {
    // Uncertain if need to remove change listeners before deleting object
    if (prioritySliders != null) {
      for (JSlider prioritySlider : prioritySliders) {
        ChangeListener[] clist = prioritySlider.getChangeListeners();
        if (clist != null) {
          prioritySlider.removeChangeListener(clist[0]);
        }
      }
    }

    setPrioritySliders(indNames);
    for (JSlider prioritySlider : prioritySliders) {
      prioritySlider.addChangeListener(cListener);
    }
  }
  
  public void updateResults() {
    resPanel.setVisible(true);
    this.pack();
  }
  
  public void hideResults() {
    resPanel.setVisible(false);
    this.pack();
  }
  
  public void setIndicatorBound(int maxV) {
    if (maxV <= 0)
      maxV = 1;
    numIndicatorSpin.setModel(new SpinnerNumberModel(maxV/2+1,1,maxV,1));
  }
  
  public int[] getPriorities() {
    int[] priorities = new int[prioritySliders.length];
    for (int i = 0; i < priorities.length; i++) {
      priorities[i] = prioritySliders[i].getValue();
    }
    return priorities;
  }
  
  public int getNumIndicators() {
    return ((Number)numIndicatorSpin.getValue()).intValue();
  }
  
  public int getNumRuns() {
    return ((Number)numRunSpin.getValue()).intValue();
  }
  
  public int getAreaType() {
    if (areaTypeBox.getModel().getSelectedItem().equals(AREA_OPTS[MIN_IND]))
      return MIN_AREA;
    else
      return MEAN_AREA;
  }
  
  public String getAltContribution() {
    return alternativesBox.getModel().getSelectedItem().toString();
  }
  
  public String getIndContribution() {
    return indicatorsBox.getModel().getSelectedItem().toString();
  }
  
  public int getDistType() {
    if (distTypeBox.getModel().getSelectedItem().equals(DIST_OPTS[PDF_IND]))
      return PDF;
    else
      return CDF;
  }
  
  public void showAboutFrame() {
    aboutFrame.pack();
    aboutFrame.setLocation( this.getLocation().x+20,
                      this.getLocation().y+20);
    aboutFrame.setVisible(true);
  }
  
  public Object[][] getCombinedRankData() {
    int numMeanRows = meanTable.getModel().getRowCount();
    int numMinRows = minTable.getModel().getRowCount();
    int numRows = Math.max(numMeanRows, numMinRows);
    int numMeanCols = meanTable.getModel().getColumnCount();
    int numMinCols = minTable.getModel().getColumnCount();
    Object[][] data = new Object[numRows][numMeanCols + numMinCols + 1];
    for (int row = 0; row < numRows; row++) {
      for (int col = 0; col < numMinCols; col++) {
        if (row < numMinRows)
          data[row][col] = minTable.getModel().getValueAt(row, col);
        else
          data[row][col] = "";
      }
      data[row][numMinCols] = "";
      for (int col = 0; col < numMeanCols; col++) {
        if (row < numMeanRows)
          data[row][col+numMinCols+1] = meanTable.getModel().getValueAt(row, col);
        else
          data[row][col] = "";
      }
    }
    return data;
  }
  
  public String[] getCombinedRankCols() {
    int numMeanCols = meanTable.getModel().getColumnCount();
    int numMinCols = minTable.getModel().getColumnCount();
    String[] cols = new String[numMeanCols + numMinCols + 1];
    for (int i = 0; i < numMinCols; i++) {
      cols[i] = minTable.getModel().getColumnName(i);
    }
    cols[numMinCols] = "";
    for (int i = 0; i < numMeanCols; i++) {
      cols[i+numMinCols+1] = meanTable.getModel().getColumnName(i);
    }
    
    return cols;
  }
  
  private JMenuBar initMenuBar() {
    JMenuBar menuBar = new JMenuBar();
    
    JMenu infoMenu = initInfoMenu();
    menuBar.add(infoMenu);
    
    return menuBar;
  }
  
  private JMenu initInfoMenu() {
    JMenu infoMenu = new JMenu();
    infoMenu.setText("Help");
    
    inputFileFormatItem = new JMenuItem();
    inputFileFormatItem.setText("Input File Format");
    
    aboutItem = new JMenuItem();
    aboutItem.setText("About...");
    
    initAboutFrame();
    
    infoMenu.add(aboutItem);
    infoMenu.add(inputFileFormatItem);
    
    return infoMenu;
  }
  
  private void initAboutFrame() {
    aboutFrame = new JFrame();
    aboutFrame.setTitle("About");
    aboutFrame.setResizable(false);
    
    StyleContext sc = new StyleContext();
    final DefaultStyledDocument doc = new DefaultStyledDocument(sc);
    JTextPane pane = new JTextPane(doc);
    
    final Style headingStyle = sc.addStyle("Heading2", null);
    headingStyle.addAttribute(StyleConstants.Foreground, Color.BLACK);
    headingStyle.addAttribute(StyleConstants.Background, Color.LIGHT_GRAY);
    headingStyle.addAttribute(StyleConstants.FontSize, new Integer(16));
    headingStyle.addAttribute(StyleConstants.FontFamily, "serif");
    headingStyle.addAttribute(StyleConstants.Bold, true);
    headingStyle.addAttribute(StyleConstants.Italic, true);
    
    final Style boldStyle = sc.addStyle("Heading3", null);
    boldStyle.addAttribute(StyleConstants.Foreground, Color.BLACK);
    boldStyle.addAttribute(StyleConstants.Background, Color.LIGHT_GRAY);
    boldStyle.addAttribute(StyleConstants.FontSize, new Integer(12));
    boldStyle.addAttribute(StyleConstants.FontFamily, "serif");
    boldStyle.addAttribute(StyleConstants.Bold, true);
    
    final String text = "Tradeoff Identification in Comparative LCA\n\n" +
                  "Valentina Prado Lopez, Thomas P. Seager PhD, Lise Laurin, \nMikhail Chester PhD, Erdem Arslan PhD\n\n" +
                  "Programmed By: \n  Nagarjuna Thottempudi and Isonify, LLC, Mark Ison, GM.\n\n" +
                  "Arizona State University\n\nDate: 04/07/2014\n\nSoftware Version: 1.0.5-alpha";
    
    try {
      SwingUtilities.invokeAndWait(new Runnable() {
        @Override
        public void run() {
          try {
            doc.insertString(0,text,null);
          
            doc.setParagraphAttributes(0,1,headingStyle,false);
            doc.setParagraphAttributes(50,1,boldStyle,false);
            doc.setParagraphAttributes(120,1,boldStyle,false);
          } catch (BadLocationException e) {
          }
        }
      });
    } catch (Exception e) {
      showMessage("Exception creating about frame","About Page Error",JOptionPane.ERROR_MESSAGE);
    }
    
    aboutFrame.getContentPane().add(new JScrollPane(pane));
    //aboutFrame.setVisible(false);


  }
  
  /**
   * Initialize data panel for gui loading data and saving rankings
   * 
   * @return data panel for loading, presenting, and saving data
   */
  private JPanel initDataPanel() {
    // Init global components
    initLoadDialog();
    initLoadButton();
    initDataTables();
    
    // Init subpanels
    JPanel rankingPanel = initRankingPanel();
    JPanel optionsPanel = initOptionsPanel();
    JPanel resultsPanel = initResultsPanel();
    //Put options in a scrollbar
    //JScrollPane optionsScroll = new JScrollPane(optionsPanel);
    //optionsScroll.setViewportView(optionsPanel);
    //optionsScroll.setBorder(BorderFactory.createEmptyBorder());
    //optionsScroll.setVisible(true);
    
    // Miscellaneous
    JLabel logo = initLogo();
    JLabel footer = initFooter();
    
    JPanel leftPanel = new JPanel();
    
    GroupLayout layout = new GroupLayout(leftPanel);
    leftPanel.setLayout(layout);
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
             .addGroup(layout.createParallelGroup()
              .addComponent(loadDataButton, (int) (0.10*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.10*windowWidth))
              .addComponent(dataTableScroll, (int) (0.45*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.45*windowWidth))
             )
             .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
             //.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED,(int)(this.screenSize.getWidth()/3),800)
              .addGroup(layout.createSequentialGroup()
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, (int) (0.40*windowWidth),(int)(0.75*windowWidth))
                .addComponent(logo, (int) (0.15*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.15*windowWidth))
              )
              .addGroup(layout.createSequentialGroup()
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, (int) (0.10*windowWidth),(int)(0.10*windowWidth))
                .addComponent(optionsPanel,(int) (0.45*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.45*windowWidth) )
              )             
             )              
            )
            .addGroup(layout.createSequentialGroup()
              .addComponent(rankingPanel, (int) (0.45*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.45*windowWidth))
              .addComponent(resultsPanel, (int) (0.55*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.55*windowWidth) )
            )
            .addGroup(layout.createSequentialGroup()
              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED,(int) (0.25*windowWidth),(int)(0.25*windowWidth))
              .addComponent(footer, (int) (0.50*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.50*windowWidth) )
              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED,(int) (0.25*windowWidth),(int)(0.25*windowWidth))
            )
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup()
                .addComponent(loadDataButton)
                .addComponent(logo)  
            )
            .addGroup(layout.createParallelGroup()
              .addComponent(dataTableScroll, (int) (0.35*windowHeight), GroupLayout.DEFAULT_SIZE, (int) (0.35*windowHeight))
              .addComponent(optionsPanel, (int) (0.35*windowHeight), GroupLayout.DEFAULT_SIZE, (int) (0.35*windowHeight))
            )
            .addGroup(layout.createParallelGroup()
              .addComponent(rankingPanel, (int)(0.45*windowHeight), GroupLayout.DEFAULT_SIZE, (int) (0.45*windowHeight))
              .addComponent(resultsPanel, (int)(0.45*windowHeight), GroupLayout.DEFAULT_SIZE, (int) (0.45*windowHeight))
            )  
            .addGap(0,20,(int)(this.screenSize.getWidth()/3))
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(footer)
            )
    );
    return leftPanel;
  }
  
  private void initLoadButton() {
    loadDataButton = new JButton();
    loadDataButton.setText("Load Data...");
  }
  
  private void initDataTables() {
    dataTable = new JTable();
    //dataTableScroll = new JScrollPane();
    dataTableScroll = initEmptyTable(dataTable);
    dataTableScroll.setBorder(BorderFactory.createTitledBorder("Loaded LCA Data"));
    dataTableScroll.setVisible(false);
    
    minTable = new JTable();
    //minTableScroll = new JScrollPane();
    minTableScroll = initEmptyTable(minTable);
    minTableScroll.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), AREA_OPTS[MIN_IND]));
    
    meanTable = new JTable();
    //meanTableScroll = new JScrollPane();
    meanTableScroll = initEmptyTable(meanTable);
    meanTableScroll.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), AREA_OPTS[MEAN_IND]));

    altRankTable = new JTable();
    altRankTableScroll = initEmptyTable(altRankTable);
    altRankTableScroll.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), "Alternative Ranking Distributions"));

  }
  
  private JLabel initLogo() {
    JLabel logo = new JLabel("",
            new ImageIcon(getClass().getResource("/org/asu/lca/images/iconSmall.jpg")),
            JLabel.CENTER);
    return logo;
  }
  
  private JLabel initFooter() {
    JLabel name = new JLabel("School of Sustainable Engineering and the Built Environment, Arizona State University");
    return name;
  }
  
  private JPanel initOptionsPanel() {
    optPanel = new JPanel();
    
    JTabbedPane tabPane = new JTabbedPane();
    //Dimension sz = new Dimension((int)(0.25*windowWidth), (int) (0.25*windowHeight));
    //tabPane.setMinimumSize(sz);
    //tabPane.setMaximumSize(sz);
    
    JPanel defPanel = initGenSettingPanel();
    tabPane.addTab("Settings", null, defPanel, "Edit General Settings");
    tabPane.setMnemonicAt(0, KeyEvent.VK_1);
    
    JPanel priPanel = initPriorityPanel();
    tabPane.addTab("Priorities", null, priPanel, "Edit Indicator Priorities");
    tabPane.setMnemonicAt(1, KeyEvent.VK_2);
    
    optPanel.add(tabPane);
    optPanel.setVisible(false);
    
    return optPanel;
  }
  
  private JPanel initGenSettingPanel() {
    JPanel settings = new JPanel();
    
    // Select area type
    JLabel areaTLabel = new JLabel("Area Type");
    areaTypeBox = new JComboBox();
    areaTypeBox.setModel(new DefaultComboBoxModel(AREA_OPTS));
    //areaTypeBox.setMaximumSize(new Dimension((int)(this.screenSize.getWidth()/5), 25));
    
    // Select number of indicators
    JLabel numIndLabel = new JLabel("Number of Indicators");
    numIndicatorSpin = new JSpinner();
    numIndicatorSpin.setToolTipText("Indicators used in LCA calculations");
    setIndicatorBound(0);
    //numIndicatorSpin.setMaximumSize(new Dimension((int)(this.screenSize.getWidth()/5), 25));
    
    // Select number of Monte Carlo Runs
    JLabel numRunLabel = new JLabel("Monte Carlo Runs");
    numRunSpin = new JSpinner(new SpinnerNumberModel(1000,1,50000,100));
    numRunSpin.setToolTipText("Number of runs to use in LCA calculations");
    //numRunSpin.setMaximumSize(new Dimension((int)(this.screenSize.getWidth()/5), 25));
    
    // Distribution types for plotting
    JLabel distTypeLabel = new JLabel("Distribution");
    distTypeBox = new JComboBox();
    distTypeBox.setModel(new DefaultComboBoxModel(DIST_OPTS));
    //distTypeBox.setMaximumSize(new Dimension((int)(this.screenSize.getWidth()/5), 25));

    // Button to run analysis
    runAnalysisButton = new JButton();
    runAnalysisButton.setText("Run Analysis");
    
    GroupLayout layout = new GroupLayout(settings);
    settings.setLayout(layout);
    
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGroup(layout.createParallelGroup()
                .addComponent(areaTLabel)
                .addComponent(numIndLabel)
                .addComponent(numRunLabel)
                .addComponent(distTypeLabel)
              )
              .addGroup(layout.createParallelGroup()
                .addComponent(areaTypeBox, (int)(this.screenSize.getWidth()/10), GroupLayout.DEFAULT_SIZE, (int)(this.screenSize.getWidth()/10))
                .addComponent(numIndicatorSpin, (int)(this.screenSize.getWidth()/10), GroupLayout.DEFAULT_SIZE, (int)(this.screenSize.getWidth()/10))
                .addComponent(numRunSpin, (int)(this.screenSize.getWidth()/10), GroupLayout.DEFAULT_SIZE, (int)(this.screenSize.getWidth()/10))
                .addComponent(distTypeBox, (int)(this.screenSize.getWidth()/10), GroupLayout.DEFAULT_SIZE, (int)(this.screenSize.getWidth()/10))
              )
            )
            .addComponent(runAnalysisButton)
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup() 
              .addComponent(areaTLabel)
              .addComponent(areaTypeBox, 25, GroupLayout.DEFAULT_SIZE, 25)
            )
            .addGroup(layout.createParallelGroup() 
              .addComponent(numIndLabel)
              .addComponent(numIndicatorSpin, 25, GroupLayout.DEFAULT_SIZE, 25)
            )
            .addGroup(layout.createParallelGroup() 
              .addComponent(numRunLabel)
              .addComponent(numRunSpin, 25, GroupLayout.DEFAULT_SIZE, 25)
            )
            .addGroup(layout.createParallelGroup() 
              .addComponent(distTypeLabel)
              .addComponent(distTypeBox, 25, GroupLayout.DEFAULT_SIZE, 25)
            )
            .addComponent(runAnalysisButton)
    );
    
    
    return settings;
  }
  
  private JPanel initPriorityPanel() {
    JPanel priPanel = new JPanel();
    
    priorityScroll = new JScrollPane();
    setPrioritySliders(new String[0]);
    //priorityButton = new JButton("Update Plots");
    
    GroupLayout layout = new GroupLayout(priPanel);
    priPanel.setLayout(layout);
    
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
      .addComponent(priorityScroll)
      //.addComponent(priorityButton)
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
      .addComponent(priorityScroll)
      //.addComponent(priorityButton)
    );
    
    return priPanel;
  }
  
  private JPanel initRankingPanel() {
    rankPanel = new JPanel();
    
    saveRankButton = new JButton();
    saveRankButton.setText("Save Rankings");
    
    rankPanel.setBorder(BorderFactory.createTitledBorder("Indicator Rankings"));
    
    GroupLayout layout = new GroupLayout(rankPanel);
    rankPanel.setLayout(layout);
    
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createSequentialGroup()
            .addComponent(minTableScroll)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
              .addComponent(meanTableScroll)
              .addComponent(saveRankButton)
            )
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup() 
              .addComponent(minTableScroll)
              .addComponent(meanTableScroll))
            .addComponent(saveRankButton)
    );
    
    rankPanel.setVisible(false);
    return rankPanel;
  }
  
  private JPanel initResultsPanel() {
    //0.55*windowWidth
    //0.45*windowHeight
    resPanel = new JPanel();
    
    JTabbedPane tabPane = new JTabbedPane();
    
    JComponent defPanel = initComparePanel(); 
    tabPane.addTab("Comparison", null, defPanel, "Compare Alternatives");
    tabPane.setMnemonicAt(0, KeyEvent.VK_4);
    
    JComponent rPanel = initAltRankPanel();
    tabPane.addTab("Ranking", null, rPanel, "Rank Alternatives");
    tabPane.setMnemonicAt(1, KeyEvent.VK_5);
    
    JComponent contPanel = initContributionPanel();
    tabPane.addTab("Contribution", null, contPanel, "Compare Contributions from Each Indicator");
    tabPane.setMnemonicAt(1, KeyEvent.VK_6);
    
    JComponent wPanel = initWeightsPanel();
    tabPane.addTab("Weights", null, wPanel, "Weight Distributions in Analysis");
    tabPane.setMnemonicAt(0, KeyEvent.VK_7);
    
    resPanel.add(tabPane);
    resPanel.setVisible(false);
    
    return resPanel;
  }
  
  private JPanel initWeightsPanel() {
    JPanel weightPanel = new JPanel();
    
    weightButton = new JButton("Visualize Weights");
    
    weightLabel = new JLabel("Weight Graphs");

    GroupLayout layout = new GroupLayout(weightPanel);
    weightPanel.setLayout(layout);
    
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
          .addGroup(layout.createSequentialGroup()
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, (int) (0.70*0.55*windowWidth), (int) (0.80*0.55*windowWidth))
            .addComponent(weightButton, (int) (0.20*0.55*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.20*0.55*windowWidth))
          )
          .addComponent(weightLabel)
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(weightButton)
          .addComponent(weightLabel)
    );
    
    return weightPanel;
  }
  
  private JPanel initComparePanel() {
    //0.55*windowWidth
    //0.45*windowHeight  
    JPanel compPanel = new JPanel();
    
    compareButton = new JButton("Graph Popup");
    compareLabel = new JLabel("Comparison Graph");
    
    GroupLayout layout = new GroupLayout(compPanel);
    compPanel.setLayout(layout);
    
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
          .addGroup(layout.createSequentialGroup()
            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, (int) (0.70*0.55*windowWidth), (int) (0.80*0.55*windowWidth))
            .addComponent(compareButton, (int) (0.20*0.55*windowWidth), GroupLayout.DEFAULT_SIZE, (int) (0.20*0.55*windowWidth))
          )
          .addComponent(compareLabel)
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(compareButton)
          .addComponent(compareLabel)
    );
    
    return compPanel;
  }
  
  private JPanel initContributionPanel() {
    JPanel contPanel = new JPanel();
    
    contLabel = new JLabel("Contribution Plots");
    
    // Select alternatives to plot
    JLabel altLabel = new JLabel("Alternative");
    alternativesBox = new JComboBox();
    alternativesBox.setToolTipText("Choose alternative to analyze");
    alternativesBox.setMaximumSize(new Dimension((int)(this.screenSize.getWidth()/5), 25));
    
    // Select indicators to plot
    JLabel indLabel = new JLabel("Indicator");
    indicatorsBox = new JComboBox();
    indicatorsBox.setToolTipText("Choose indicator to analyze");
    indicatorsBox.setMaximumSize(new Dimension((int)(this.screenSize.getWidth()/5), 25));
    
    setContributionOptions(new String[] {}, new String[] {});
    
    
    
    // Button only needed for gnuplot
    contButton = new JButton("Plot");
    
    GroupLayout layout = new GroupLayout(contPanel);
    contPanel.setLayout(layout);
    
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGroup(layout.createParallelGroup()
                .addComponent(altLabel)
                .addComponent(alternativesBox)
              )
              .addGroup(layout.createParallelGroup()
                .addComponent(indLabel)
                .addComponent(indicatorsBox)
              )
              .addComponent(contButton)
            )
            .addComponent(contLabel)
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup() 
              .addComponent(altLabel)
              .addComponent(indLabel)
            )
            .addGroup(layout.createParallelGroup() 
              .addComponent(alternativesBox)
              .addComponent(indicatorsBox)
              .addComponent(contButton)
            )
            .addComponent(contLabel)
    );
    
    
    return contPanel;
  }
  
  private JPanel initAltRankPanel() {
    JPanel altRankPanel = new JPanel();
    
    altRankButton = new JButton("Visualize Table");
            
    GroupLayout layout = new GroupLayout(altRankPanel);
    altRankPanel.setLayout(layout);
    
    layout.setAutoCreateGaps(true);
    layout.setAutoCreateContainerGaps(true);
    layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
          .addComponent(altRankTableScroll)
          //.addGroup(layout.createSequentialGroup()
            //.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED, this.screenSize.width/3, this.screenSize.width/2)
          .addComponent(altRankButton)
          //)
    );
    layout.setVerticalGroup(layout.createSequentialGroup()
          .addComponent(altRankTableScroll)
          .addComponent(altRankButton)
    );
            
    return altRankPanel;
  }
  
  private JScrollPane initEmptyTable(JTable tmpTab) {

    tmpTab.setModel(new javax.swing.table.DefaultTableModel( new Object[0][0], new String[0]));
    tmpTab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
    tmpTab.getAccessibleContext().setAccessibleDescription("");
    //tmpTab.setRowHeight(18);
    tmpTab.setFillsViewportHeight(true);
    JScrollPane tmpScroll = new JScrollPane(tmpTab);
    tmpScroll.setViewportView(tmpTab);
    tmpScroll.setBorder(BorderFactory.createEmptyBorder());
    return tmpScroll;
  }
  
  /**
   * Initialize JFileChooser for loading and saving data
   */
  private void initLoadDialog() {
    fileChooser = new JFileChooser();
    fileChooser.setAcceptAllFileFilterUsed(false);
    fileChooser.setDialogTitle("Select Data File");
    fileChooser.setFileFilter(new ExcelFileFilter());
  }
  
  private Dimension setTableDim(int maxW, int maxH, JTable table) {
    int tabWidth = table.getModel().getColumnCount()*79;
    int tabHeight = table.getRowHeight()*(table.getModel().getRowCount()+3)+5;
    int width = Math.min(tabWidth,maxW);
    int height = Math.min(tabHeight, maxH);
    return  new Dimension(width, height);
  }
  
  private void setPrioritySliders(String[] indNames) {
    prioritySliders = new JSlider[indNames.length];
    String[] tickLabels = LCAmodel.PRIORITY_NAMES;
    
    //Dimension setSize = new Dimension(40, 20);
    
    JPanel pPanel = new JPanel();
    pPanel.setLayout(new GridBagLayout());
    
    GridBagConstraints c = new GridBagConstraints();
    
    for (int i = 0; i < indNames.length; i++) {
      //System.out.println(indNames[i]);
      JLabel priorityLabel = new JLabel(indNames[i]);
      prioritySliders[i] = new JSlider(JSlider.HORIZONTAL, 1, tickLabels.length, tickLabels.length/2+1);
      prioritySliders[i].setMajorTickSpacing(1);
      prioritySliders[i].setPaintTicks(true);
      prioritySliders[i].setLabelTable(null);
      
      //prioritySliders[i].setMaximumSize(setSize);
      //prioritySliders[i].setMinimumSize(setSize);
      
      //c.fill = GridBagConstraints.HORIZONTAL;
      c.gridx = 0;
      c.gridy = i;
      pPanel.add(priorityLabel, c); 
      c.gridx = 1;
      pPanel.add(prioritySliders[i], c);
    }
    
    if (indNames.length > 0) {
      Hashtable<Integer, JLabel> labelTable = new Hashtable<>();
      for (int i = 0; i < tickLabels.length; i++) {
        labelTable.put(new Integer(i+1), new JLabel(tickLabels[i]));
      }
      prioritySliders[indNames.length-1].setLabelTable(labelTable);
      prioritySliders[indNames.length-1].setPaintLabels(true);
    }
    
    priorityScroll.setViewportView(pPanel);
    pPanel.setAutoscrolls(true);

    priorityScroll.setMaximumSize(new Dimension((int)(screenSize.getWidth()/2), (int)(screenSize.getHeight()/4)));
    priorityScroll.setBorder(BorderFactory.createEmptyBorder());
    this.pack();
  }
  
  private File createTempFile(String partName, String endName) {
    File tempOutputFile;
    try {
      tempOutputFile = File.createTempFile(partName,endName);
    } catch (IOException ex) {
      Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
    tempOutputFile.deleteOnExit();
    return tempOutputFile;
  }
  
  /**
   * Copies outranking data to temp file
   * @param altNames
   * @param fDistribution
   * @return outranking temp file
   */
  private File rankToFile(double[][] rankHist) {
    File tempOutputFile = createTempFile("altranking", ".dat");
    
    //System.out.println(tempOutputFile.toString());
    try (PrintWriter pw = new PrintWriter(tempOutputFile)) 
    {
      pw.print("#Rank\n");

      for(int j = 0; j < rankHist[0].length; j++) {
        for (int a = 0; a < 4; a++) {
          double l = j - 0.25 + 0.5*Math.floor(a/2);
          for (int b = 0; b < 2; b++) {
            for (int i = 0; i < rankHist.length; i++) {
              double k = i-0.25+0.5*b;
              pw.print((l+1)+"\t");
              pw.print((k+1) + "\t");
              if (a==0 || a == 3 || b == 0)
                pw.print(0+"\t");
              else
                pw.print(rankHist[i][j]+"\t");
            }
            pw.print("\n");
            
            for(int i = 0; i < rankHist.length; i++) {
              double k = i-0.25+0.5*b;
              pw.print((l+1)+ "\t");
              pw.print((k+1)+"\t");
              if (a > 0 && a < 3 && b==0)
                pw.print(rankHist[i][j]+"\t");
              else
                pw.print(0+"\t");
            }
            pw.print("\n");
          }
          pw.print("\n");
        }
      }
      
      pw.close();
    }
    catch (FileNotFoundException ex) {
      Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
    return tempOutputFile;
  }
  
  /**
   * Copies outranking data to temp file
   * @param altNames
   * @param fDistribution
   * @return outranking temp file
   */
  private File distToFile(String[] altNames, double[][] fDistribution, double maxX) {
    File tempOutputFile = createTempFile("outranking", ".dat");
    
    //System.out.println(tempOutputFile.toString());
    try (PrintWriter pw = new PrintWriter(tempOutputFile)) 
    {
      pw.print("index\t");
      for(int i = 0; i < altNames.length; i++) {
        if(i != altNames.length-1)
          pw.print(altNames[i]+"\t");
        else
          pw.println(altNames[i]);
      }

      for(int i = 0; i < fDistribution.length; i++) {
        pw.print(((double)i/fDistribution.length*maxX)+"\t");
        for(int j = 0; j < fDistribution[0].length; j++) {
          if(j != fDistribution[0].length-1)
            pw.print(fDistribution[i][j]+"\t");
          else
            pw.println(fDistribution[i][j]);
        }
      }
      pw.close();
    }
    catch (FileNotFoundException ex) {
      Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
    return tempOutputFile;
  }
  
  private File plotData(double[][] data, String[] labels, int maxX, String title, boolean newWindow) {
    File tempInputFile = distToFile(labels, data,maxX);
    File tempOutputFile = null;
    if (!newWindow) {
      tempOutputFile = createTempFile("savePlot", ".png");
    }
    if (tempInputFile != null) {
      plotDistribution(tempInputFile, tempOutputFile, title, maxX);
    }
    return tempOutputFile;
  }
  
    /**
   * Gnuplot test method. Move to appropriate place later
   * @param tempOutputFile is mandatory
   */
  private void plotDistribution(File tempInputFile, File tempOutputFile, String plotTitle, double maxX) {
    if(tempInputFile == null) {
        System.out.println("Main file is empty. Cannot plot");
        return;
    }
    
    File tempScriptFile = createTempFile("gnuplotscript", ".txt");
    try (PrintWriter pw = new PrintWriter(tempScriptFile)) {
        if (tempOutputFile == null)
          pw.println("set terminal wxt enhanced persist");
        else {
          pw.println("set term pngcairo size " + screenSize.width/3 + "," + screenSize.height/4 + " enhanced color");
          //check opeating system
          String outputFileName = tempOutputFile.toString();
          if(LCAcontroller.isWindows()) {
            outputFileName = outputFileName.replace("\\","\\\\");
          }
          pw.println("set out \"" + outputFileName.toString() + "\"");
        }
        pw.println("set font 'Verdana,10'");
        pw.println("set style fill transparent solid 0.50 border -1");
        //pw.println("set key title \"Alternatives\"");
        pw.println("set key inside left top vertical Left reverse enhanced autotitles nobox");
        pw.println("set key noinvert samplen 1 spacing 1 width 0 height 0 ");
        //pw.println("set palette rgb 33,13,10;");
        pw.println("set title \""+plotTitle+"\""); 
        pw.println("set xrange [0:" + maxX + "]");
        StringBuilder plotData;
        try (Scanner s = new Scanner(tempInputFile)) {
          String titleLine;
          plotData = new StringBuilder();
          plotData.append("plot ");
          if(s.hasNext()) {
            titleLine = s.nextLine();
            String[] parameters = titleLine.split("\t");
            //pw.println("set pal maxcolors " + (parameters.length-1));
            for(int i = 1; i < parameters.length; i++) {
              String tempFileName = tempInputFile.toString();
              if(LCAcontroller.isWindows()) {
                tempFileName = tempFileName.replace("\\","\\\\");
              }
              plotData.append("\"").append(tempFileName)
                      .append("\" using 1:").append(i+1).append(" title ");
              if(getDistType()==PDF)
                plotData.append("\"").append(parameters[i]).append("\" smooth sbezier w filledcurves y1=0 lt ").append(i);
              else {
                //Cumulative distribution, no colour filling under curve
                plotData.append("\"").append(parameters[i]).append("\" smooth sbezier lt ").append(i);
              }
              if(i != parameters.length-1)
                plotData.append(", ");
              else
                plotData.append("\n");
            }
          }
          s.close();
        }
        pw.println(plotData.toString());
        pw.close();
    }
    catch (FileNotFoundException ex) {
      Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
      return;
    }
   
   //show terminal
   if(tempOutputFile == null) 
    showGnuPlot(tempScriptFile, true);
   else
    showGnuPlot(tempScriptFile, false);
  }
  
  /**
   * File chooser filter for accepting only excel files
   */
  class ExcelFileFilter extends javax.swing.filechooser.FileFilter {
    
    @Override
    public boolean accept(File file) {
      // Allow only files with .xls or .xlsx extension, or directories
      return file.isDirectory() || file.getAbsolutePath().endsWith(".xls") ||
              file.getAbsolutePath().endsWith(".xlsx");
    }
    
    @Override
    public String getDescription() {
      // Displayed in dialog
      return "Excel Documents (*.xls) or (*.xlsx)";
    }
    
  }

  private void plot3DRankings(File tempInputFile, String[] labels) {
    if(tempInputFile == null) {
        System.out.println("Main file is empty. Cannot plot");
        return;
    }
   
    File tempScriptFile = createTempFile("gnuplotscript", ".txt");
    try (PrintWriter pw = new PrintWriter(tempScriptFile)) {
        // Adding ranking plots
        //pw.println("set terminal wxt 1 size 640,480 enhanced font 'Verdana,10' persist");
        pw.println("set terminal wxt enhanced persist");
        pw.println("set font 'Verdana,10'");
        pw.println("reset");
        pw.println("unset key");
        pw.println("unset colorbox");
        pw.println("set view 60,210");
        pw.println("set style fill transparent solid 0.70 border -1");
        pw.println("set pm3d depthorder hidden3d");
        pw.println("set pm3d implicit");
        pw.println("unset hidden3d");
        pw.println("set grid x y z");
        //pw.println("set zrange [0:1]");
        //pw.println("set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB");
        pw.println("set palette rgb 33, 13, 10");
        pw.println("set xlabel \"Rank\"");
        pw.println("set ylabel \"Alternative\"");
        pw.println("set zlabel \"Percent\"");
        pw.println("set title \"Alternative Ranking Distributions\"");
        pw.println("set pal maxcolors "+labels.length);
        pw.println("set ticslevel 0");
        pw.println("set xtics 1,1,"+labels.length);
        // Set y tics based on length of labels
        String yTics = "set ytics (";
        for (int i = 0; i < labels.length; i++) {
          yTics+= "\"" + labels[i] + "\" " + (i+1);
          if (i < labels.length-1)
            yTics+=",";
        }
        yTics+= ")";
        pw.println(yTics);
        String tempFileName = tempInputFile.toString();
        if(LCAcontroller.isWindows()) {
          tempFileName = tempFileName.replace("\\","\\\\");
        }

        String cmd = "splot \"" + tempFileName + "\" u ";
        for (int i = 0; i < labels.length; i++) {
          int start = i*3+1;
          cmd+= start + ":" + (start+1) + ":" + (start+2) +  ":(" + i + ") w pm3d";
          if (i < labels.length-1)
            cmd+= ", \\";
          else
            cmd+= " lt -1";
          pw.println(cmd);
          cmd = "\"\" u ";
        }
        
        pw.close();
    }
    catch (FileNotFoundException ex) {
      Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
      return;
    }
    
    showGnuPlot(tempScriptFile, true);
  }
  
  private void showGnuPlot(File tempScriptFile, boolean isNewWindow) {
    if(LCAcontroller.isWindows()) {
        try {
            //Process p = Runtime.getRuntime().exec("gnuplot/bin/gnuplot gnuplot/bin/script.txt");
            Process p = Runtime.getRuntime().exec("gnuplot/bin/gnuplot "+tempScriptFile.toString());
            if(!isNewWindow)
                p.waitFor();
        } catch (IOException ex) {
               System.out.println("Error: "+ ex.getMessage());
        } catch (InterruptedException ex) {
            Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
        }
    } else if(LCAcontroller.isMac()) {
        System.out.println("Mac OS");  
    } else if(LCAcontroller.isUnix()) {
        String software = "gnuplot";
        // Bug here on linux, gnuplots freeze the screen until they are all closed...
        Process p;
        try {
           p = Runtime.getRuntime().exec(software+" "+tempScriptFile.toString());
           if(!isNewWindow)
               p.waitFor();
        } catch (IOException ex) {
            System.out.println("error message: "+ex.getMessage());
            if(ex.getMessage().contains("error=2, No such file or directory")) {
                String message = "\""+software+"\" not found. Please install"
                        + " or configure path and retry";
                String title = "Software Not detected";
                showMessage(message, title, JOptionPane.ERROR_MESSAGE);
            }
         } catch (InterruptedException ex) {
            Logger.getLogger(LCAview.class.getName()).log(Level.SEVERE, null, ex);
        }
    } else if(LCAcontroller.isSolaris()) {
        System.out.println("Solaris");
    } else {
        System.out.println("Unknown OS");      
    } 
  }
}
  