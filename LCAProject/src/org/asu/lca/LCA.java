/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Launch4j 
 * innoscript
 */

package org.asu.lca;

/**
 * This is the main driver class to run LCA Tools
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Apr 6, 2014
 */
public class LCA {
  public static void main(String[] args) {
    LCAview theView = new LCAview();
    LCAmodel theModel = new LCAmodel();
    LCAcontroller theController = new LCAcontroller(theView, theModel);
    
    theView.setVisible(true);
  }
}
