/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca.smaa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;
import org.asu.lca.LCAmodel;

/**
 * The <code>Comparison</code> class provides functionality for LCA Comparisons.
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Feb 23, 2014
 * 
 * 3/2/2014 (nagarjuna): added Outranking
 */
public class Comparison {
//implements Observer
  
  //create Singleton Class
  private static final int MIN_IND = 0;
  private static final int MEAN_IND = 1;
  private static final int NUM_RANKINGS = 2;
  
  //distribution type
  private static final int PROBABILITY_DISTRIBUTION = LCAmodel.PDF; 
  private static final int CUMULATIVE_DISTRIBUTION = LCAmodel.CDF; 
  
  private static final int PDF_IND = 0;
  private static final int CDF_IND = 1;
  
  private static final String FILEMAP_POST_ALT = "Alt";
  private static final String FILEMAP_POST_IND = "Ind";
  
  // Private variables
  private TreeMap<String,Alternative> alternatives;
  private ArrayList< ArrayList <Rank> > indicatorRankings;
  private String[] useIndicators;
  
  //stores file index name for combination of alternative name and 
  //criterion name
  private final HashMap<String, Integer> fileIndexMap = new HashMap<>();
  private final HashMap<Integer, String> tempFileMap = new HashMap<>();
  //File name prefix for temp flow files
  private final String fNamePrefix = "tempFlow";
  
  private double[][][] flowDists; // PDF and CDF of flow distributions
  
  private SMAA_TY ty; // Weights during outranking algorithm
  private double[][] rankHist; // Distribution of alternative rankings based on flows
  private double[][] indicatorImpact; // Impact of indicators on each alternative
  
  private int numRuns; // Number of monte carlo runs
  private static final int noOfBins = 100; //Number of bins for computing netflow distributions
  
  /**
   * The constructor initializes the comparison with empty set of alternatives
   *
   **/
  //private Comparison() {
  //For MVC model
  public Comparison() {
    init(1000,1);
  }
  
  public Comparison(int nRuns)
  {
    init(nRuns,1);
  }
  
  public Comparison(int nRuns, int nPriorities) {
    init(nRuns, nPriorities);
  }
  
  public Object[][] getData() {
    String[] inds = getCommonIndicators();
    String[] alts = getAlternativeNames();
    Object[][] dat = new Object[inds.length][alternatives.size()*2+2];
    
    for (int i = 0; i < inds.length; i++) {
      String ind = inds[i];
      dat[i][0] = ind;
      dat[i][1] = getAlternative(alts[0]).getIndicator(ind).getUnits();
      for (int j = 0; j < alternatives.size(); j++) {
        Indicator tmpInd = getAlternative(alts[j]).getIndicator(ind);
        dat[i][(j+1)*2] = String.format("%.3e",tmpInd.getMean());
        dat[i][(j+1)*2+1] = String.format("%.3e",tmpInd.getSD());
      }
    }
    return dat;
  }
  
  public String[] getColumnTypes() {
    String[] alts = getAlternativeNames();
    String[] colNames = new String[alts.length*2+2];
    colNames[0] = "Indicator";
    colNames[1] = "Units";
    
    for (int i = 0; i < alts.length; i++) {
      colNames[(i+1)*2] = (alts[i] + ":Mean");
      colNames[(i+1)*2+1] = (alts[i] + ":SD");
    }
    
    return colNames;
  }
  
  public String[] getIndividualDistNames(String[] altNames, String[] indNames) {
   //create new names from the combination of alternativeNames and indicatorNames for plotting
    String[] combinedNames = new String[altNames.length*indNames.length];
    int i = 0;
    for(String alt: altNames) {
        for(String ind: indNames) {
            combinedNames[i++] = alt+":"+ind;
        }
    }

    return combinedNames;

  }
  
  public double[][] getIndividualDists(String[] altNames, String[] indNames, int distType) {
        System.out.println("Number of Runs: "+ numRuns + " altLength: "+ altNames.length);
        //change the flows size to accomodate for individual plots
        double[][] flows = new double[numRuns][altNames.length*indNames.length];
        for (double[] fl : flows)
          for (int i = 0; i < fl.length; i++)
            fl[i] = 0.;
        
        int index = 0;
        for(String alt: altNames) {
            for(String ind: indNames) {
                String fileName = tempFileMap.get(
                        fileIndexMap.get(ind+FILEMAP_POST_IND)*this.getAlternativeNames().length + fileIndexMap.get(alt+FILEMAP_POST_ALT));
              try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
                String nextLine;
                int i = 0;
                while((nextLine = br.readLine()) != null) {
                  Double value = Double.parseDouble(nextLine);
                  //System.out.println("value: "+ value);
                  flows[i++][index] = value;
                }
                br.close();
              }
              catch (FileNotFoundException ex) {
                  Logger.getLogger(Comparison.class.getName()).log(Level.SEVERE, null, ex);
              } catch (IOException ex) {
                  Logger.getLogger(Comparison.class.getName()).log(Level.SEVERE, null, ex);
              }  

              index++;
            }
        }
        
        return setFlowDistribution(normalizeFlows(flows), distType);
  }
  
  public double[][] getComparisonDists(int distType) {
    int distInd = CDF_IND;
    if (distType == PROBABILITY_DISTRIBUTION)
      distInd = PDF_IND;
    
    return flowDists[distInd];
  }
  
  public double[][] getWeightDists(int distType) {
    int distInd = CDF_IND;
    if (distType == PROBABILITY_DISTRIBUTION)
      distInd = PDF_IND;
    
    return setFlowDistribution(this.ty.getNormalizedWeights(), distType);
  }
  
  /**
   * Compare alternatives using multicriteria preference flows
   * @param nRuns Number of monte carlo runs to use
   */
  public void compareAlternatives(int nRuns) {
    setFlows(this.getAlternativeNames(), this.getSelectedIndicators(), nRuns);
  }
  
  /**
   * Compare alternatives using multicriteria preference flows
   *
   */
  public void compareAlternatives() {
    setFlows(this.getAlternativeNames(), this.getSelectedIndicators(), this.numRuns);
  }
  
  /**
   * Get rankings based on minimum area overlap of all alternatives
   * 
   * @return matrix of objects indicating rank, indicator name, and area value
   */
  public Object[][] getMinRanking() {
    return getRanking(MIN_IND);
  }
  
  /**
   * Get rankings based on mean area overlap of all alternatives
   * 
   * @return matrix of objects indicating rank, indicator name, and area value
   */
  public Object[][] getMeanRanking() {
    return getRanking(MEAN_IND);
  }
  
  /**
   * Get rankings based on type area overlap of all alternatives
   * 
   * @param type Type of method to use, either Comparison.MIN or Comparison.MEAN
   * 
   * 
   * @return matrix of objects indicating rank, indicator name, and area value
   */
  public Object[][] getRanking(int type) {
    //displayRankings();
    
    if (type < 0 || type >= NUM_RANKINGS)
      type = MIN_IND;
    
    if (indicatorRankings.isEmpty() || indicatorRankings.get(type).size() != getCommonIndicators().length)
        setRankings();
    
    ArrayList<ArrayList<String>> rank = new ArrayList<>();
    for (int i = 0; i < indicatorRankings.get(type).size(); i++) {
      rank.add(new ArrayList<String>());
      rank.get(i).add(Integer.toString(indicatorRankings.get(type).get(i).getRank()));
      rank.get(i).add(indicatorRankings.get(type).get(i).getName());
      rank.get(i).add(String.format("%.3e",indicatorRankings.get(type).get(i).getArea()));
    }
    
    int rows = rank.size();
    int cols = rank.get(0).size();
    Object[][] result = new Object[rows][cols];
    for(int i = 0 ; i < rows; i++)
      result[i] = rank.get(i).toArray();
            
    return result;
  }
  
  public double[][] getAltHistData() {
    double[][] data = new double[rankHist.length][rankHist.length];
    for (int i = 0; i < rankHist.length; i++) {
      for (int j = 0; j < rankHist.length; j++) {
        data[i][j] = rankHist[i][j]*100;
      }
    }
    return data;
  }
  
  public Object[][] getAltRanking() {
    String[] altNames = getAlternativeNames();
    Object[][] data = new Object[altNames.length][altNames.length+1];
    for (int i = 0; i < altNames.length; i++) {
      data[i][0] = altNames[i];
      for (int j = 0; j < altNames.length; j++) {
        data[i][j+1] = String.format("%.2f%%",rankHist[i][j]*100);
      }
    }
    return data;
  }
  
  
  /**
   * Rank all common indicators for given alternatives, and store the rankings 
   * for retrieval with getRankings
   * 
   **/
  public void setRankings() {
    indicatorRankings.clear();

    int sz = alternatives.size();
    String[] altNames = getAlternativeNames();
    String[] indNames = getCommonIndicators(altNames);
    
    ArrayList<ArrayList<Double>> areas = getIndicatorOverlaps(altNames, indNames);
    
    // Make sure sizes are as expected...
    if (areas.size() == indNames.length) {
      
      // Sort by min and mean area for each indicator
      for (int i = 0; i < 2; i++)
        indicatorRankings.add(new ArrayList<Rank>());
      
      
      // Modified by mison to fix bug if ever the same areas...
      TreeMap<Double,Rank> meanRankTree = new TreeMap<>();
      TreeMap<Double, Rank> minRankTree = new TreeMap<>();
      for (int i = 0; i < indNames.length; i++) {
        //System.out.println(areas.get(i));
        Rank minRank = new Rank(indNames[i],getMinArea(areas.get(i)));
        Rank meanRank = new Rank(indNames[i],getMeanArea(areas.get(i)));
        //System.out.println(minHash + "\t" + meanHash);
        meanRankTree.put(meanRank.getHashDouble(), meanRank);
        minRankTree.put(minRank.getHashDouble(), minRank);
      }
      
      // Iterate through sorted map to store rankings
      Iterator<Double> minTIter = minRankTree.keySet().iterator();
      Iterator<Double> meanTIter = meanRankTree.keySet().iterator();
      
      int rank = 1;
      while(meanTIter.hasNext()){
        Rank meanR= meanRankTree.get(meanTIter.next());
        meanR.setRank(rank);
        Rank minR = minRankTree.get(minTIter.next());
        minR.setRank(rank);
        indicatorRankings.get(MIN_IND).add(minR);
        indicatorRankings.get(MEAN_IND).add(meanR);
        rank++;
      }
    }
    displayRankings();
  }
  
  /**
   * Display the rankings for both min and mean just for testing
   * 
   */
  public void displayRankings()
  {
    if (!indicatorRankings.isEmpty())
    {
      // Display Rankings just for testing
      System.out.println("RANKINGS:");
      for (int i = 0; i < indicatorRankings.size(); i++) {
        
        System.out.println(i);
        for (int j = 0; j < indicatorRankings.get(i).size(); j++) {
          Rank r = indicatorRankings.get(i).get(j);
          System.out.println("\tRank: " + r.getRank() + " Name: " + r.getName() + " Area: " + r.getArea());
        }
      }
    }
  }   
  
  /**
   * Add a new alternative with given name.
   *
   * @param name String name of alternative
   * 
   * @return Alternative created so that indicators may be added
   **/
  public Alternative addAlternative(String name) {
    name = Alternative.formatName(name);
    alternatives.put(name,new Alternative(name));
    return getAlternative(name);
  }
  
  /** 
   * Remove any alternative with the given name
   * 
   * @param name String name of indicator to remove
   */
  public void removeAlternative(String name) {
    alternatives.remove(Alternative.formatName(name));
  }
  
  /**
   * Get the alternative with the given name
   * 
   * @param name String name of alternative to retrieve
   * 
   * @return Alternative on success, NULL on failure
   */
  public Alternative getAlternative(String name) {
    return alternatives.get(Alternative.formatName(name));
  }
  
  /**
   * Get all alternative names
   * 
   * @return Array of string alternative names stored in comparison
   */  
  public String[] getAlternativeNames() {
    return alternatives.keySet().toArray(new String[0]);
  }
  
    /**
   * Get the selected indicators for Outranking
   * 
   * @return List of Strings of selected indicators
   */
  public String[] getSelectedIndicators() {
    if (this.useIndicators.length == 0)
      return getCommonIndicators();
    return this.useIndicators;
  }
  
  /**
   * Set the specific indicators for Outranking
   * 
   * @param numInds number of indicators to use
   * @param type type of ranking to use (Comparison.MIN or Comparison.MEAN)
   */
  public void selectIndicators(int numInds, int type) {
    if (this.indicatorRankings.isEmpty())
      setRankings();
    
    if (type < 0 || type >= NUM_RANKINGS)
      type = MEAN_IND;
    
    if (numInds < 1 || numInds > this.indicatorRankings.get(type).size())
      this.useIndicators = getCommonIndicators();
    else {
      this.useIndicators = new String[numInds];
      for (int i = 0; i < numInds; i++) {
        this.useIndicators[i] = this.indicatorRankings.get(type).get(i).getName();
      }
      this.ty.setWeights(numInds, 1);
    }
  }
  
  public void setNumRuns(int nRuns) {
    if (nRuns > 0)
      this.numRuns = nRuns;
  }
  
  public boolean setIndicatorPriorities(int[] priorities) {
    return this.ty.setPriorities(priorities);
  }
  
  /**
   * Get the indicators which are common to all alternatives
   * 
   * @return List of Strings of common indicator names among all alternatives
   */
  public String[] getCommonIndicators() {
    return getCommonIndicators(this.getAlternativeNames());
  }
  
  /**
   * Get the indicators which are common to the entire list of alternatives
   * 
   * @param altNames Array of strings of alternative names to use.
   * 
   * @return List of Strings of common indicator names among alternatives
   */
  public String[] getCommonIndicators(String[] altNames) {
    //System.out.println("Alternative Names: "+ Arrays.toString(altNames));
    int sz = altNames.length;
    TreeMap<String,Integer> hist = new TreeMap<>();
    for (String altName : altNames) {
      String[] indNames = getAlternative(altName).getIndicatorNames();
      //System.out.println("Indicator Names: "+ Arrays.toString(indNames));
      for (String indName : indNames) {
        Integer val = hist.get(indName);
        if (val == null)
          val = 0;
        hist.put(indName, ++val);
      }
    }
    Iterator<String> histIter = hist.keySet().iterator();
    ArrayList<String> goodInds = new ArrayList<>();
    while(histIter.hasNext()){
      String key = histIter.next();
      //System.out.println("key: " + key + " value: " + hist.get(key));
      if (hist.get(key) == sz)
        goodInds.add(key);
    }
    
    return goodInds.toArray(new String[0]);
    
  }
  
  //public void init(int nRuns) {
  private void init(int nRuns, int nPriorities) {
    this.alternatives = new TreeMap<>();
    this.indicatorRankings = new ArrayList<>();
    this.useIndicators = new String[0];
    this.ty = new SMAA_TY(0,1,nPriorities);
    //flows = new Double[0][0];
    //flowDistribution = new Double[0][0];
    rankHist = new double[0][0]; // Distribution of alternative rankings based on flows
    indicatorImpact = new double[0][0]; // Impact of indicators on each alternative
    if (nRuns < 0)
      this.numRuns = 1000;
    else
      this.numRuns = nRuns;
    
  }
  
  /**
   * Get mean of ArrayList
   * 
   * @param areas List of doubles associated with area
   * 
   * @return double mean of list
   */
  static private double getMeanArea(ArrayList<Double> areas) {
    double sum = 0;
    //System.out.println("Mean areas: "+ areas.toString());
    if(!areas.isEmpty()) {
      for (Double mark : areas) {
        sum += mark;
      }
      //System.out.println("Mean area: "+ (sum/areas.size()));
      return sum / areas.size();
    }
    return sum;
  }
  
  /**
   * Get minimum value in list
   * 
   * @param areas List of doubles associated with areas
   * 
   * @return minimum value in list
   */
  static private double getMinArea(ArrayList<Double> areas) {
    double dMin = 2;
    //System.out.println("Minimum areas: "+ areas.toString());
    if(!areas.isEmpty()) {
      for (Double mark : areas) {
        if (mark < dMin)
          dMin = mark;
      }
      //System.out.println("min: "+ dMin);
      return dMin;
    }
    return -1;
  }
  
  /**
   * Build a list of overlapping areas for all combinations of alternatives and
   * all common indicators among alternatives
   * 
   * @param altNames String array of names of alternatives to compare
   * 
   * @return Matrix of areas. NumIndicators X NumAlternativeCombinations
   */
  private ArrayList<ArrayList<Double>> getIndicatorOverlaps(String[] altNames) {
    return getIndicatorOverlaps(altNames, getCommonIndicators(altNames));
  }

  /**
   * Build a list of overlapping areas for all combinations of alternatives and
   * all common indicators among alternatives
   * 
   * @param altNames String array of names of alternatives to compare
   * @param indNames String array of names of indicators to compare
   * 
   * @return Matrix of areas. NumIndicators X NumAlternativeCombinations
   */
  private ArrayList<ArrayList<Double>> getIndicatorOverlaps(String[] altNames, String[] indNames) {
    int si = indNames.length;
    int sz = altNames.length;
    ArrayList<ArrayList<Double>> areas = new ArrayList<>();
    for (int k = 0; k < si; k++) {
      areas.add(new ArrayList<Double>());
      String goodInd = indNames[k];
      for (int i = 0; i < sz-1; i++) {
        for (int j = i+1; j < sz; j++) {
          areas.get(k).add(getAlternative(altNames[i])
                            .getIndicator(goodInd)
                              .areaOverlap(getAlternative(altNames[j])
                                            .getIndicator(goodInd)));
        }
      }
    }
    return areas;
  }
  

  
  /**
   * Computes P for outranking function
   * 
   * @param altNames String array of names of alternatives to compare
   * @param indNames String array of names of indicators to compare
   * 
   * @return double array corresponding to P for each indicator input
   * 
   * Note: The function of P may change in the future
   */
  private double[] computeAllP(String[] altNames, String[] indNames) {
    double[] Ps = new double[indNames.length];
    //System.out.println("Computing Ps...");
    for (int k = 0; k < indNames.length; k++) {
      double tmpP = 0;
      for (String altName : altNames) {
        tmpP -= getAlternative(altName).getIndicator(indNames[k]).getSD();
      }
      // P defined ad -mean(SD_k) over all alternatives
      Ps[k] = tmpP/(double)altNames.length;
      //System.out.println("P[" + indNames[k] + "] - " + Ps[k]);
    }
    return Ps;
  }
  
  private ArrayList<PrintWriter> initStorageFiles(String[] altNames, String[] indNames) {
    int altNamesLen = altNames.length;
    int indNamesLen = indNames.length;

    //store each alternative net flow with respect to each criterion in each file
    //Open file descriptors equivalant to altNamesLen*indNamesLen
    fileIndexMap.clear();
    //store altNames Index and indNames index
    // Add postfix in rare case indicator and alternative have same name
    for(int i = 0; i < altNamesLen; i++)
        fileIndexMap.put(altNames[i]+FILEMAP_POST_ALT, i);
    //store from index 0;
    for(int j = 0; j < indNamesLen; j++)
        fileIndexMap.put(indNames[j]+FILEMAP_POST_IND, j);
    //clear temp file map first
    tempFileMap.clear();
    ArrayList<PrintWriter> pWriters = new ArrayList<>();
    for(int i = 0; i < indNamesLen; i++) {
        for(int j = 0; j < altNamesLen; j++) {
            try {
                String fName =fNamePrefix+(altNamesLen*i + j);
                File f = File.createTempFile(fName, ".txt");
                f.deleteOnExit();
                tempFileMap.put((altNamesLen*i + j), f.getAbsolutePath());
                //System.out.println(fName);
                PrintWriter pw = new PrintWriter(f);
                pWriters.add(pw);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Comparison.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Comparison.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    return pWriters;
  }
  
  private double[][] setSamples(String[] altNames, String[] indNames) {
        double[][] samples = new double[altNames.length][indNames.length];
        for (int i = 0; i < altNames.length; i++)
          for (int k = 0; k < indNames.length; k++)
            samples[i][k] = getAlternative(altNames[i]).getIndicator(indNames[k]).sample();
        
        return samples;
  }
  
  private double[][] getNetIndividualFlows(double[][] samples, double[] weights, double[] Ps) {
        //store final net flows per criteria
        double[][] netIndividualFlow = new double[samples.length][weights.length];
        
        // Initialize to 0
        for(int i = 0; i < samples.length; i++)
          for(int k = 0; k < weights.length; k++)
            netIndividualFlow[i][k] = 0;
        
        //run half of matrix - fill current and alternative indices
        //In one run fills (i,j) and (j,i) and so on.
        for(int i = 0; i < samples.length; i++) {
            for(int j = i+1; j < samples.length; j++) {
                for(int k = 0; k < weights.length; k++) {
                   double P = Ps[k];
                   double Q = P/2;
                   
                   //first alternative from previously sampled data
                   double samp_i = samples[i][k];
                   //second alternative
                   double samp_j = samples[j][k];
                   double diff = samp_i-samp_j;
                   
                   double tmpFlow_i = getOutranking(diff,P, Q)*weights[k];
                   // Use same weights and samples for both comparisons
                   // -diff = samp_j - samp_i
                   double tmpFlow_j = getOutranking(-diff,P,Q)*weights[k];
                   netIndividualFlow[i][k] += (tmpFlow_i - tmpFlow_j);
                   netIndividualFlow[j][k] += (tmpFlow_j - tmpFlow_i);
                }
            }
        }
        return netIndividualFlow;
  }
  
  private double[] storeFlows(ArrayList<PrintWriter> pWriters, double[][] netIndividualFlow) {
    int altNamesLen = netIndividualFlow.length;
    int indNamesLen = netIndividualFlow[0].length;
    double[] curFlows = new double[altNamesLen];
    
    double sum = 0;
        for(int i = 0; i < altNamesLen; i++) {
            double nFlow = 0.0;
            for(int j = 0; j < indNamesLen; j++) {
                nFlow += netIndividualFlow[i][j];
                pWriters.get(j*altNamesLen + i).println(netIndividualFlow[i][j]/((double)altNamesLen - 1));
            }
            curFlows[i] = nFlow/((double)altNamesLen - 1);                   
            
            // Check validity
            if((Math.abs(curFlows[i]) > 100.0000001)) 
               System.out.println("Net Flow Violation: "+ curFlows[i]);
            
            sum += curFlows[i];
        }
        if (Math.abs(sum) > 0.000001)
          System.out.println("Sum Net Flow Violation: " + sum);
        
        return curFlows;
  }
  
  /**
   * Computes multi-criteria preference flows for the selected alternatives and criteria
   * 
   * @param altNames String array of names of alternatives to compare
   * @param indNames String array of names of indicators to compare
   * @param nRuns Number of monte carlo runs to use in computations
   */
  private void setFlows(String[] altNames, String[] indNames, int nRuns) {
    if (nRuns > 0)
      this.numRuns = nRuns;
    
    // Generate new set of SMAATY weights, keeping same priorities
    this.ty.setWeights(indNames.length, numRuns);
    
    //stores summation of final values for each alternative
    double[][] flows = new double[numRuns][altNames.length];
    
    // Compute P for each indicator
    double[] Ps = computeAllP(altNames, indNames);
    
    ArrayList<PrintWriter> pWriters = initStorageFiles(altNames, indNames);
    
    for(int runs = 0; runs < numRuns; runs++) {
        double[] weights = ty.getWeights(runs);
        
        // Use the same samples for each monte carlo run, all comparisons
        double[][] samples = setSamples(altNames, indNames);
        
        double[][] netIndividualFlow = getNetIndividualFlows(samples, weights, Ps);

        double[] curFlows = storeFlows(pWriters, netIndividualFlow);
        flows[runs] = curFlows.clone();
    }
    
    //close all print writers
    for(int i = 0; i < pWriters.size(); i++)
      pWriters.get(i).close();
    
    //Normalize the net flows and get distribution
    normalizeFlows(flows);
    
    flowDists = new double[2][][];
    double[][] tmpDist = setFlowDistribution(flows, PROBABILITY_DISTRIBUTION);
    flowDists[PDF_IND] = tmpDist.clone();
    tmpDist = setFlowDistribution(flows,CUMULATIVE_DISTRIBUTION);
    flowDists[CDF_IND] = tmpDist.clone();
    
    rankAlternatives(flows);
    
    //File tempOutputFile = copyOutRankingDataToFile(altNames, 
    //        setFlowDistribution(normalizeFlows(flows), PROBABILITY_DISTRIBUTION));
    
    // Create temporary file for rankings to plot. Hacky now just for demo. Clean up later.
    /*File tempRankFile = null;
    try {
        tempRankFile = File.createTempFile("altranking",".dat");
        tempRankFile.deleteOnExit();
        System.out.println(tempRankFile.toString());
        PrintWriter pw = new PrintWriter(tempRankFile);
        pw.print("#Rank\t");
        for(int i = 0; i < altNames.length; i++) {
          if(i != altNames.length-1)
            pw.print(altNames[i]+"\t");
          else
            pw.println(altNames[i]);
        }
        // rank Hist stored alternative along rows, rank along cols
        for(int j = 0; j < rankHist[0].length; j++) {
          for (int a = 0; a < 4; a++) {
            double l = j - 0.25 + 0.5*Math.floor(a/2);
            for (int b = 0; b < 2; b++) {
              for (int i = 0; i < rankHist.length; i++) {
                  double k = i-0.25+0.5*b;
                  pw.print((l+1) +"\t");
                  pw.print((k+1) +"\t");
                  if (a == 0 || a == 3 || b==0)
                    pw.print(0+"\t");
                  else
                    pw.print(rankHist[i][j]+"\t");
              }
              pw.print("\n");
              for (int i = 0; i < rankHist.length; i++) {
                  double k = i-0.25+0.5*b;
                  pw.print((l+1) +"\t");
                  pw.print((k+1) +"\t");
                  if (a > 0 && a < 3 && b==0)
                    pw.print(rankHist[i][j]+"\t");
                  else
                    pw.print(0+"\t");
              }
              pw.print("\n");
            }
            pw.print("\n");
          }
        }
        pw.close();
     }
     catch (FileNotFoundException ex) {
        Logger.getLogger(Comparison.class.getName()).log(Level.SEVERE, null, ex);
     } catch (IOException ex) {
          Logger.getLogger(Comparison.class.getName()).log(Level.SEVERE, null, ex);
     }*/
    
    //rankIndicatorImpact(flows);

    /**
     * Gnuplot test clean this up later!
     */
    //plotDistribution(tempOutputFile, tempRankFile, PDF_PLOT_TITLE);
  }
    
  /**
   * Rank alternatives based on flow rankings for each run. Creates a 2D histogram
   * with first component representing alternative name, second component representing 
   * ranking distribution.
   */
  private void rankAlternatives(double[][] flows) {
    if (flows == null || flows.length == 0)
        return;
      
    int altNamesLen = flows[0].length;
    
    // Create 2D histogram for rankings
    rankHist = new double[altNamesLen][altNamesLen];
    for (int i = 0; i< altNamesLen; i++)
      for (int j = 0; j < altNamesLen; j++)
        rankHist[i][j] = 0.0;
    
    for (double[] flow : flows) {
      TreeMap<Double,Rank> ranking = new TreeMap<>();
      for (int i = 0; i < altNamesLen; i++) {
        Rank r = new Rank(((Integer)i).toString(), flow[i]);
        ranking.put(r.getHashDouble(), r);
        //System.out.println(i + ": " + altNames[i] + " " + this.flows[runs][i] + " " + r.getHashDouble());
      }
      Iterator<Double> iter = ranking.keySet().iterator();
      int rank = altNamesLen-1;
      while(iter.hasNext()){
        this.rankHist[Integer.parseInt(ranking.get(iter.next()).getName())][rank--]++;
      } 
    }
    
    //String[] altNames = this.getAlternativeNames();
    for (int i = 0; i < altNamesLen; i++) {
      //System.out.println(altNames[i]);
      for (int j = 0; j < altNamesLen; j++) {
        rankHist[i][j] /= this.numRuns;
      }
      //System.out.println(Arrays.toString(rankHist[i]));
    }
    
  }

  /**
   * Compute outranking value using linear function, where negative value 
   * results in higher outranking
   * 
   * @param value difference between two samples for outranking
   * @param P Set point P in outranking function
   * @param Q Set point Q in outranking function
   * 
   * @return Outranking value between 0 and 1
   * 
   * NOTE: This function could change
   */
  private double getOutranking(double value, double P, double Q) {
    //Overlap function- Need to confirm with Valentina
    //Current rule 
    // This is non decreasing function
    //OverLapFunction = 0 if x >= Q
    //OverLapFunction = (x-Q)/(P-Q) if Q >= x >= P
    //OverLapFunction = 1 if x <= P
    double outrank;
    if(value >= Q)
        outrank = 0.0;
    else if(value < Q && value > P)
        outrank= (Q-value)/(Q-P);
    else
        outrank = 1.0;
    //System.out.println("Diff: " + value + " - " + outrank);
    return outrank;
  }
  
  /**
   * Function for normalizing the netflow values
   * 
   * No parameters, as min and max are known to be -100 and 100, respectively
   * @param minAltValue stores min value of each alternative
   * @param maxAltValue stores max value of each alterative
   */
  private double[][] normalizeFlows(double[][] currentFlows) { //double[] minAltValue, double[] maxAltValue) {
      //System.out.println("rows: "+ flows.length + " columns: "+ flows[0].length);
    double minAltValue = -100.0;
    double maxAltValue = 100.0;
    for (double[] flow : currentFlows) {
      for (int j = 0; j < flow.length; j++) {
        flow[j] = (flow[j] - minAltValue) / (maxAltValue-minAltValue);
        // Problem with rounding due to double not able to represent -100/100 perfectly, maps to -7E-17 or 1+1E-14.
        if (flow[j] < 0 && flow[j] > -1E-14) {            
          //System.out.println("Problem with rounding: "+ flow[j]);
          flow[j] = 0.;
        } else if (flow[j] > 1 && flow[j] < 1+1E-14) {
          //System.out.println("Problem with rounding: "+ flow[j]);
          flow[j] = 1.;
        }
      }
    }
    return currentFlows;
  }
  
  /**
   * Divide the net flows into bins and count the values 
   * for plotting probability distributions
   * All alternatives are in range [0,1]
   * @param: currenFlows -Normalized flows
   * @param: distr distribution type(P.D.F, C.D.F)
   */
  private double[][] setFlowDistribution(double[][] currentFlows, int distr) {
    if(noOfBins == 0) {
        return null;
    }
    double[][] flowDistribution = new double[noOfBins][currentFlows[0].length];
    double[] total = new double[currentFlows[0].length];
    
    // Initialize
    for (double[] fd : flowDistribution) {
      for (int j = 0; j < fd.length; j++) {
        total[j] = 0.;
        fd[j] = 0.;
      }
    }
    
    for (double[] flow : currentFlows) {
      for (int j = 0; j < currentFlows[0].length; j++) {
        int scaledValue = (int) Math.floor(flow[j] * noOfBins);
        if (scaledValue > noOfBins || scaledValue < 0) {
          System.out.println("Look further: flows: " + flow[j] + "scaled value: " + scaledValue);
          continue;
        }
        if(scaledValue == noOfBins)
          flowDistribution[noOfBins-1][j]++;
        else
          flowDistribution[scaledValue][j]++;
      }
    }

    for (double[] fd : flowDistribution) {
        for (int j = 0; j < flowDistribution[0].length; j++) {
            if (distr == PROBABILITY_DISTRIBUTION) {
                fd[j] = fd[j] / currentFlows.length;
            } else {
                //CUMULATIVE_DISTRIBUTION
                total[j] += (fd[j] / currentFlows.length);
                fd[j] = total[j];
            }
            //flows.length is monte carlo runs
        }
    }
    return flowDistribution;
  }
  

  
   
    /**
     * generates sub plots according to type requested
     * @param alternativeNames
     * @param selectedIndicators 
     */
    private void generateDistributionPlot(String[] alternativeNames, String[] selectedIndicators, int plotType) {
    }
    
    
            //print flows
            //for(int i = 0; i < numRuns; i++)
              //  System.out.println(Arrays.toString(flows[i]));

            //if(plotType == PROBABILITY_DISTRIBUTION) {
                //File tempOutputFile = copyOutRankingDataToFile(combinedNames, 
                //        setFlowDistribution(normalizeFlows(flows), PROBABILITY_DISTRIBUTION)); 
                //plotDistribution(tempOutputFile, null, "PDF");
            //} else {
                //File tempOutputFile = copyOutRankingDataToFile(combinedNames, 
                //        setFlowDistribution(normalizeFlows(flows), CUMULATIVE_DISTRIBUTION)); 
                //plotDistribution(tempOutputFile, null, "CDF");
            //}
    //}
    
      /**
   * Rank indicator impact based on correlation between smaa-ty weights and 
   * resulting flows
   * 
   * NOTE: Still in development
   */
  private void rankIndicatorImpact(double[][] flows) {
      if (flows == null || flows.length == 0)
        return;
        //this.compareAlternatives();
      
      String[] indNames = this.getSelectedIndicators();
      
      int nRuns = flows.length;
      int altNamesLen = flows[0].length;
      int indNamesLen = indNames.length;
      
      this.indicatorImpact = new double[indNamesLen][altNamesLen];
      for (int i = 0; i < indNamesLen; i++) {
        // For each alternative, compute mutual information between smaaty weights and flow
        for (int a = 0; a < altNamesLen; a++) {
          // Flow is between -100 and 100, Weights between 0 and 100
          double[] thisFlow = new double[numRuns];
          double[] thisWeight = new double[numRuns];
         
          for (int w = 0; w < nRuns; w++) {
            thisFlow[w] = flows[w][a];
            thisWeight[w] = ty.getWeights(w)[i];
          }
          indicatorImpact[i][a] = this.spearmanRank(thisFlow, thisWeight);
          
        }
      }
      
      //String[] altNames = this.getAlternativeNames();
      for (int i = 0; i < indNamesLen; i++) {
        ArrayList<Double> arrInd = new ArrayList<>();
        for (int a = 0; a < altNamesLen; a++) {
          //System.out.println(indNames[i] + ": " + altNames[a] + " = " + indicatorImpact[i][a]);
          arrInd.add(indicatorImpact[i][a]);
        }
          
        System.out.println(indNames[i] + ": " + getMeanArea(arrInd));
      }
        
      
  }
  
  private double spearmanRank(double[] x, double[] y) {
    if (x.length != y.length)
      return 0;
    
    SpearmansCorrelation spear = new SpearmansCorrelation();
    return spear.correlation(x, y);
  }
  
  
  /**
   * Mutual information for the given joint histogram and number of total tallys
   * in the histogram
   * 
   * @param x Flows from -100 to 100
   * @param y Weights from 0 to 100
   * @return mutual information of x and y
   */
  private double mutualInformation(double[] x, double[] y) {
    
          double[][] hist = new double[101][101];
          for (double[] hist1 : hist) {
            for (int c = 0; c < hist1.length; c++) {
              hist1[c] = 0;
            }
          }
          
          for (int w = 0; w < x.length; w++) {
            hist[(int)((x[w]+100)/2)][(int)(y[w])]++;
          }
          
          double[] Pr = new double[hist.length];
          double[] Pc = new double[hist[0].length];
          double Hr = 0;
          double Hc = 0;
          double Hrc = 0;
          for (int r = 0; r < hist.length; r++) {
            Pr[r] = 0;
            for (int c = 0; c < hist[r].length; c++) {
              if (r == 0) {
                Pc[c] = 0;
              }
              hist[r][c] /= x.length;
              Pr[r] += hist[r][c];
              Pc[c] += hist[r][c];
              Hrc += -hist[r][c]*Math.log(hist[r][c]+0.000000001)/Math.log(2);
            }
          }
          for (int r = 0; r < Pr.length; r++)
            Hr += -Pr[r]*Math.log(Pr[r]+0.000000001)/Math.log(2);
          for (int c = 0; c < Pc.length; c++)
            Hc += -Pc[c]*Math.log(Pc[c]+0.000000001)/Math.log(2);
          
          double MI = Hr + Hc - Hrc;
          return Math.sqrt(MI*MI/Hr/Hc);
  }
}
