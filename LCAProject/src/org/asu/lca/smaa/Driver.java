/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package org.asu.lca.smaa;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;

/** Simple driver class for testing
 *
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version February 9 2014
 */
public class Driver {

  /**
   * <code>main</code> function simply provides testing for program
   * 
   * @param args the command line arguments which are not used
   * 
   * Nothing returned, outputs presented via command line
   */
  public static void main(String[] args) { 
    // TODO code application logic here    
    /*Comparison c = Comparison.getInstance();
    c.init(1000);
    Alternative a1 = c.addAlternative("A");
    a1.addIndicator("Azul", 1.13, 1.1, .175);
    a1.addIndicator("Rojo", 2.61, 2.47, .557);
    a1.addIndicator("Amarillo", 4210, 4060, 877);
    a1.addIndicator("Blanco",.138,.124,.0645);
    
    a1 = c.addAlternative("B");
    a1.addIndicator("Azul", .971, .977, .115);
    a1.addIndicator("Rojo", 2.19, 2.12, .512);
    a1.addIndicator("Amarillo", 4650, 4560, 944);
    a1.addIndicator("Blanco",.119,.102,.0524);
    
    a1 = c.addAlternative("C");
    a1.addIndicator("Azul", 1.5, 1.491008, .165);
    a1.addIndicator("Rojo", 3, 2.950478, .552);
    a1.addIndicator("Amarillo", 6000, 5950, 780);
    a1.addIndicator("Blanco",0.092,.073219,.07);
    
    a1 = c.addAlternative("D");
    a1.addIndicator("Azul", 0.8, 0.784467, 0.16);
    a1.addIndicator("Rojo", 1.99, 1.871303, 0.72);
    a1.addIndicator("Amarillo", 4220, 4136.928, 850);
    a1.addIndicator("Blanco",.105,.08583,.074);
    
    
    c.setRankings();
    
    c.selectIndicators(2,Comparison.MIN);
    String inds[] = c.getSelectedIndicators();
    for (String ind : inds) {
      System.out.println(ind);
    }*/
    
    /*SMAA_TY tyW = new SMAA_TY(4,10000,5);
    int priorities[] = {5, 4, 4, 3};
    tyW.setPriorities(priorities);
    
    for (int i = 0; i < tyW.getNumSamples(); i++)
      System.out.println(Arrays.toString(tyW.getWeights(i)) + ";");  

    */
    
    String test = "test.xlsx";
    String replace = test.replace(".", "rank.");
    System.out.println(replace);
    
  }
  

}
