/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca.smaa;

/**
 * The <code>Rank</code> class simply stores the ranking data for each 
 * Indicator.
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Feb 9, 2014
 */
public class Rank {
  // Private variables
  private int theRank; // Ranking
  private double value; // Value for ranking
  private String name; // Name of indicator
  
  public Rank(String name, double value, int rank) {
    this.name = Indicator.formatName(name);
    this.theRank = rank;
    this.value = value;
  }
  
  public Rank(String name, double value) {
    this.name = Indicator.formatName(name);
    this.theRank = 0;
    this.value = value;
  }
 
  /**
   * <code>getName</code> returns name of the indicator associated with ranking.
   *
   * @return String name
   **/ 
  public String getName() {
    return this.name;
  }
  
  /**
   * <code> setRank </code> sets the rank
   * @param rank Rank for it
   */
  public void setRank(int rank) {
    this.theRank = rank;
  }
  
  /**
   * <code>getRank</code> returns ranking of the indicator.
   *
   * @return integer ranking
   **/
  public int getRank() {
    return this.theRank;
  }
  
  /**
   * <code>getArea</code> returns the area of overlap of the indicator.
   *
   * @return double area of overlap
   **/
  public double getValue() {
    return this.value;
  }
  
  public double getArea() {
    return this.value;
  }
  
  public String getHash() {
    return getHashString();
  }
  
  public String getHashString() {
    //System.out.println(String.format("%.32f", this.value) + new Integer(Math.abs(this.name.hashCode())).toString());
    return String.format("%.32f", this.value) + new Integer(Math.abs(this.name.hashCode())).toString();
    //do not format - Need to format because of some very small numbers, especially in outranking.
    // Default precision with %f was 6, and many numbers ended up appearing as 0 when trying to hash them.  
    // Turns out 15 wasn't even enough, so increased to 32.  Uncomment the println to check it (particularly with last
    // alternative, hashed as 53).
    
    //return String.format("%f", this.value) + new Integer(Math.abs(this.name.hashCode())).toString();
  }
  
  public Double getHashDouble() {
    return Double.parseDouble(getHashString());
  }

}
