/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package org.asu.lca.smaa;

import org.apache.commons.math3.distribution.*;
// Remove whitespace and capitalize words
import org.apache.commons.lang3.text.WordUtils;

/**
 * The <code>Indicator</code> class provides functionality for indicators
 * (also: impact categories, criteria) for LCA Comparisons.
 *
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Feb 22, 2014
 * 
 * Edits: 2/22 mison - Reimplement original Indicator class with bug fix for
 *                     AreaOverlap when sig1 == sig2
 *                     Also include units
 */
public class Indicator extends LogNormalDistribution {
  
  // Private variables
  private final String name;
  private final String units;
  private final double median;
  
  //Monte carlo runs
  private static final Integer monteCarloRuns =100000;
  private static final Integer noOfBins = 50;
  
  /**
   * The constructor initializes the indicator as a log-normal distribution 
   * with a given arithmetic mean, median and standard deviation.
   *
   * @param name String name of indicator
   * @param units Units used by indicator
   * @param mean Arithmetic mean of distribution
   * @param median Arithmetic median of distribution
   * @param std Arithmetic standard deviation of distribution
   **/
  public Indicator(String name, String units, double mean, double median, double std) {
    // Convert mu and sigma to geometric for initialization
    super(getMu(mean,std),getSigma(mean,std));
    this.name = formatName(name);
    this.units = units;
    this.median = median;
  }
  
    /**
   * The constructor initializes the indicator as a log-normal distribution 
   * with a given arithmetic mean, median and standard deviation.
   *
   * @param name String name of indicator
   * @param mean Arithmetic mean of distribution
   * @param median Arithmetic median of distribution
   * @param std Arithmetic standard deviation of distribution
   **/
  public Indicator(String name, double mean, double median, double std) {
    // Convert mu and sigma to geometric for initialization
    super(getMu(mean,std),getSigma(mean,std));
    this.name = formatName(name);
    this.median = median;
    this.units = "";
  }
  
      /**
   * The constructor initializes the indicator as a log-normal distribution 
   * with a given arithmetic mean and standard deviation. Median and units are 
   * not necessary for functionality 
   *
   * @param name String name of indicator
   * @param mean Arithmetic mean of distribution
   * @param std Arithmetic standard deviation of distribution
   **/
  public Indicator(String name, double mean, double std) {
    // Convert mu and sigma to geometric for initialization
    super(getMu(mean,std),getSigma(mean,std));
    this.name = formatName(name);
    this.median = -1;
    this.units = "";
  }
  
  /**
   * <code>getName</code> returns name of the indicator.
   *
   * @return String name
   **/
  public String getName() {
    return this.name;
  }

  /**
   * <code>getSD</code> returns arithmetic standard deviation of distribution.
   *
   * @return double arithmetic standard deviation
   **/
  public double getSD() {
    return Math.sqrt(this.getNumericalVariance());
  }

  /**
   * <code>getMean</code> returns arithmetic mean of distribution.
   *
   * @return double arithmetic mean
   **/
  public double getMean() {
    return this.getNumericalMean();
  }
  
  /**
   * <code>getMedian</code> returns median of distribution.
   *
   * @return double median
   **/
  public double getMedian() {
    return this.median;
  }
  
  /**
   * <code>getUnits</code> returns indicator units.
   *
   * @return String units
   **/
  public String getUnits() {
    return this.units;
  }
  
  /**
   * <code>areaOverlap</code> finds the overlapping area between two indicators.
   * 
   * PRECONDITION: ind2 must have the same name as this indicator
   * 
   * @param ind2 Indicator to compute overlap
   *
   * @return double indicating the area of overlap between the two distributions
   *         -1 returned if ind2 has different name
   **/
  public double areaOverlap(Indicator ind2) {
    // Only compute overlap if indicators have the same name
    if (this.name.compareToIgnoreCase(ind2.getName()) == 0)
    {
      return areaOverlap((LogNormalDistribution)ind2);
    }
    else // Different indicator names, so area overlap does not make sense
      return -1;
  }
  
  /**
   * <code>areaOverlap</code> finds the overlapping area between two lognormal
   * distributions.
   * 
   * @param logn Log-normal distribution to compute overlap
   *
   * @return Double indicating the area of overlap between the two distributions
   **/
  public double areaOverlap(LogNormalDistribution logn) {
    
    double thresh = 0.000001;
    // Check if analytic method will work
    if (Math.abs(this.getShape() - logn.getShape()) < thresh) {
      if (Math.abs(this.getScale() - logn.getScale()) < thresh)
        return 1; // Distributions are the same
      else
        return numericOverlap(logn);
    }
    else
      return analyticOverlap(logn);
  }
  
  
  private double analyticOverlap(LogNormalDistribution logn) {
    // Initialize variables for first distribution
    double mu1 = this.getScale();
    double sig1 = this.getShape();
    double var1 = Math.pow(sig1, 2);
    
    // Initialize variables for second distribution
    double mu2 = logn.getScale();
    double sig2 = logn.getShape();
    double var2 = Math.pow(sig2, 2);
    
    // Bug when sig1 == sig2
    if (Math.abs(var1-var2)< 0.000000000001)
      return numericOverlap(logn);
    
    // Discriminant
    double disc =  Math.pow(2*(var1*mu2-var2*mu1),2) 
                            - 4*(var2-var1)*( Math.pow(mu1,2)*var2
                                             -Math.pow(mu2,2)*var1 
                                             +2*var1*var2*(Math.log(sig1)-Math.log(sig2)));
    
    // Get 2 roots if discriminant is not negative
    if (disc >= 0) {
      double root1 = Math.exp( (1/(2*(Math.pow(sig1,2)-Math.pow(sig2,2))))
                              *(Math.sqrt(disc) + 2*(mu2*var1-mu1*var2)));
      double root2 = root1;
      if (disc > 0) {
        root2 = Math.exp( (1/(2*(Math.pow(sig1,2)-Math.pow(sig2,2))))
                         *(-Math.sqrt(disc) + 2*(mu2*var1-mu1*var2)));
        // Make root1 smallest
        if (root2 < root1) {
          double tmp = root1;
          root1 = root2;
          root2 = tmp;
        }
           
      }
      
      double cdf = 0;
      double diff = Math.min(this.getShape(), logn.getShape())/2;
      
      double check = Math.max(root1/2,root1-diff);
      //System.out.println("1: " + this.density(check) + " 2: " + logn.density(check));
      if (this.density(check) < logn.density(check)) {
        cdf+= this.cumulativeProbability(root1);
      }
      else {
        cdf+= logn.cumulativeProbability(root1);
      }
      check = Math.min(root1+diff,(root1+root2)/2);
      // If too close to zero on this intersection, move to the other one
      if (this.density(check) == 0 && logn.density(check)==0)
        check = Math.max(root2-diff, (root1+root2)/2);
      //System.out.println("1: " + this.density(check) + " 2: " + logn.density(check));
      if (this.density(check) < logn.density(check)) {
        cdf+= this.probability(root1,root2);
      }
      else {
        cdf+= logn.probability(root1,root2);
      }

      // Slight bug in that root2 is included in both estimates,
      // but should not cause any major problem
      check = root2+diff;
      //System.out.println("1: " + this.density(check) + " 2: " + logn.density(check));
      if (this.density(check) < logn.density(check)) {
        cdf+= 1-this.cumulativeProbability(root2);
      }
      else {
        cdf+= 1-logn.cumulativeProbability(root2);
      }
      
      // Testing numeric approach
      //System.out.println("CDF Analytic - Numeric: " + Math.abs(cdf - numericOverlap(logn)));
      return cdf;
    }
    else {
      // How to handle cases if zero roots?
      return -1;
    }
    
    
  }
  
    
  /**
   * <code>numericOverlap</code> finds the overlapping area between two lognormal
   * distributions when analytic solution is not available.
   * 
   * @param logn Log-normal distribution to compute overlap
   *
   * @return Double indicating the area of overlap between the two distributions
   **/
  private double numericOverlap(LogNormalDistribution logn) {
    
    // Threshold to determine numeric accuracy
    double thresh = Math.min(this.getShape(),logn.getShape())/1000;
    
    // Define anchors for binary search of intersections
    double x[] = getAnchors(logn);
    
    // Assume three possible intersection points
    double roots[] = new double[x.length-1];
    // Minimums to the left or each root
    boolean thisMin[] = new boolean[x.length]; 
    //System.out.println(x[0] + " " + x[1] + " " + x[2] + " " + x[3]);
    
    for (int i = 0; i < roots.length; i++) {
      double left = x[i];
      double right = x[i+1];
      boolean thisMinL = this.density(left) < logn.density(left);
      boolean thisMinR = this.density(right) < logn.density(right);
      while (thisMinL != thisMinR && right-left > thresh) {
        double tmp = left + (right-left)/2;
        boolean thisMinTmp = this.density(tmp) < logn.density(tmp);
        if (thisMinTmp == thisMinL)
          left = tmp;
        else
          right = tmp;
      }
      roots[i] = left;
      thisMin[i] = thisMinL;
    }
    // Find minimum to the right of the last root
    thisMin[thisMin.length-1] = this.density(x[x.length-1]) < logn.density(x[x.length-1]);
    
    //System.out.println("Roots 0: " + roots[0] + " 1: " + roots[1] + " 2: " + roots[2]);
    
    // Accumulate CDF between roots
    double cdf = 0;
    // Slight bug in that roots 1 + 2 included in two integrals,
    // but should not cause any major problem
    if (thisMin[0])
        cdf+= this.cumulativeProbability(roots[0]);
    else
        cdf+= logn.cumulativeProbability(roots[0]);

    for (int i = 1; i < roots.length; i++)
    {
      if (thisMin[i])
        cdf+= this.probability(roots[i-1],roots[i]);
      else 
        cdf+= logn.probability(roots[i-1],roots[i]);
    }
   
    if (thisMin[thisMin.length-1])
        cdf+= 1-this.cumulativeProbability(roots[roots.length-1]);
    else 
        cdf+= 1-logn.cumulativeProbability(roots[roots.length-1]);
    
    //System.out.println("Numeric Area: " + cdf);
    return cdf;
  }
  
  
  /**
   * <code>getAnchors</code> intermediate values between possible intersections
   * of this indicator and a lognormal distribution.
   * 
   * @param logn Log-normal distribution being compared
   *
   * @return double array of intermediate values, where intersections are likely
   *         between points
   **/
  private double[] getAnchors(LogNormalDistribution logn) {
    
    double x[] = new double[4];
    x[0] = 0;
    x[1] = Math.min(Math.exp(this.getScale()),Math.exp(logn.getScale()));
    x[2] = Math.max(Math.exp(this.getScale()),Math.exp(logn.getScale()));
    x[3] = Double.MAX_VALUE;
    
    // Find appropriate start and end where there is some separation between densities
    double startSD = 50;
    double stepSD = 0.5;
    double numSD = startSD;
    while (this.density(x[0]) == logn.density(x[0])) {
      x[0] = Math.max(0, Math.min(Math.exp(this.getScale() - numSD*this.getShape()), 
                                  Math.exp(logn.getScale() - numSD*logn.getShape())));
      numSD -= stepSD;
    }
    numSD = startSD;
    while (this.density(x[3]) == logn.density(x[3])) {
      x[3] = Math.max(Math.exp(this.getScale() + numSD*this.getShape()),
                      Math.exp(logn.getScale() + numSD*logn.getShape()));
      numSD -= stepSD;
    }
    
    return x;
  }
  
  /*
    DataObject do1 = generateLogNormalPDF(this.mu, this.sigma);
    DataObject do2 = generateLogNormalPDF(ind2.mu, ind2.sigma);
    
    Double min = Math.min(do1.getMin(), do2.getMin());
    Double max = Math.max(do1.getMax(), do2.getMax());
    
    //Keep min 0; --Intentional
    min = 0.0;
    
    //plot("Log Normal Distribution", do1.getValues(), min, max);
    //plot("Log Normal Distribution", do2.getValues(), min, max);
    //System.out.println("Min value: "+ min + "Max value: "+ max);

    double[] v1 = do1.getValues();
    double[] v2 = do2.getValues();

    double[] scaledValues1 = new double[v1.length];
    double[] scaledValues2 = new double[v2.length];

    double range = max-min;
    double binLength = range/noOfBins;
    //double[] scaledValues = new double[values.length];
    double[] pdfArray1 = new double[noOfBins+1];
    double[] pdfArray2 = new double[noOfBins+1];
    
    //Error condition
    if(v1.length != v2.length)
            return -1;
    
    for(int i = 0; i < v1.length; i++) {
        scaledValues1[i] = Math.ceil((v1[i] - min)/binLength);
        scaledValues2[i] = Math.ceil((v2[i] - min)/binLength);
        if(scaledValues1[i] > noOfBins) {
            System.out.println("ScaledValues1 greater: "+ scaledValues1[i]+ " v1: "+ v1[i]);
        } else {
            if((int)scaledValues1[i] == 0)
                pdfArray1[1]++;
            else
                pdfArray1[(int)scaledValues1[i]]++;
        }
        
        if(scaledValues2[i] > noOfBins) {
            System.out.println("ScaledValues2 greater: "+ scaledValues1[i]+ " v1: "+ v1[i]);
        } else {
            if((int)scaledValues2[i] == 0)
                pdfArray2[1]++;
            else
                pdfArray2[(int)scaledValues2[i]]++;
        }
    }

    double[] minArray = new double[noOfBins+1];
    double count = 0;
    for(int i = 0; i < minArray.length; i++) {
            minArray[i] = Math.min(pdfArray1[i], pdfArray2[i]);
            count = count + minArray[i];
    }
    double overlapArea = count/monteCarloRuns;

    //System.out.println("Overlap area: "+ overlapArea);	
    //For plotting X-axis will be min+i*binLength
    //Y-axis devide each by monteCarloRuns
    return overlapArea;
  }*/
  
    /**
     * Generate Probability Distribution Function for
     * Log-Normal Probability Distribution Function
     * @param mu
     * @param sigma
     * @return DataObject with Log normally distributed data
     */
   /*private DataObject generateLogNormalPDF(double mu, double sigma) {
        Random r = new Random();
        
        //System.out.println("mu: "+ mu +" sigma: "+ sigma);

        double[] values = new double[monteCarloRuns];
        Double min = Double.POSITIVE_INFINITY;
        Double max = Double.NEGATIVE_INFINITY;
        for(int i = 0; i < monteCarloRuns; i++) {
                values[i] = Math.exp(r.nextGaussian()*(sigma) + mu);//Log-Normal
                if(values[i] > max)
                        max = values[i];
                if(values[i] < min)
                        min = values[i];
        }
        DataObject d = new DataObject(values, min, max);
        //plot("Log-Normal Distribution", values, 0, max);
        return d;
    }*/
    
    /**
     * Class DataObject stores Log Normally distributed data
     */
   /* private class DataObject {
        double[] values;
        double min;
        double max;

        DataObject(double[] values, double min, double max) {
                this.values = Arrays.copyOf(values, values.length);
                this.min = min;
                this.max = max;
        }

        double[] getValues() {
                return values;
        }

        double getMin() {
                return min;
        }

        double getMax() {
                return max;
        }
    }*/

  
  /**
   * <code>getMu</code> returns the geometric mean given arithmetic 
   * mean and standard deviation inputs.  This function is used by the 
   * constructor for proper calls to super.
   * 
   * @param mean Arithmetic mean
   * @param std Arithmetic  standard deviation
   * 
   * @return The geometric mean of the distribution
   **/
  static private double getMu(double mean, double std) {
      // Entered arithmetic mean and standard deviation, so convert to mu and sigma
      // of normal distribution
      return Math.log(mean) - 0.5*Math.pow(getSigma(mean,std),2);

  }
  
  /**
   * <code>getSigma</code> returns the geometric standard deviation given
   * arithmetic mean and standard deviation inputs.  This function 
   * is used by the constructor for proper calls to super.
   * 
   * @param mean Arithmetic mean
   * @param std Arithmetic standard deviation
   *
   * @return The geometric standard deviation of the distribution
   **/
  static private double getSigma(double mean, double std) {
      // Entered arithmetic mean and standard deviation, so convert to mu and sigma
      // of normal distribution
      return Math.sqrt(Math.log(1+Math.pow(std/mean,2)));
  }
  
  /**
   * <code>formatName</code> formats name into format of indicator names.
   * 
   * @param name String potentially naming an indicator
   *
   * @return String name formatted as an indicator name
   * 
   * NOTE: This method is static, and therefore does not effect actual indicator
   **/
  static public String formatName(String name) {
    return WordUtils.capitalize(name).replace(" ", "");
  }
  
}
