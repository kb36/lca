/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package org.asu.lca.smaa;

import java.util.TreeMap;

import org.apache.commons.lang3.text.WordUtils;

/**
 * The <code>Alternative</code> class provides functionality for different
 * alternatives in LCA Comparisons.
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Feb 9, 2014
 */
public class Alternative {
  // Private variables
  private final String name;
  private final TreeMap<String,Indicator> indicators;
  
  /**
   * The constructor initializes the alternative with empty set of indicators
   *
   * @param name String name of alternative
   **/
  public Alternative(String name) {
    this.name = formatName(name);
    this.indicators = new TreeMap<>();
  }
  
  /**
   * Add a new indicator
   * with a given arithmetic mean, median and standard deviation.
   *
   * @param name String name of indicator
   * @param mean Arithmetic mean of distribution
   * @param median Arithmetic median of distribution
   * @param std Arithmetic standard deviation of distribution
   **/
  public void addIndicator(String name, double mean, double median, double std) {
    indicators.put(Indicator.formatName(name),new Indicator(name,mean,median,std));
  }
  
    /**
   * Add a new indicator
   * with a given arithmetic mean, median and standard deviation.
   *
   * @param name String name of indicator
   * @param unit String indicator units
   * @param mean Arithmetic mean of distribution
   * @param median Arithmetic median of distribution
   * @param std Arithmetic standard deviation of distribution
   **/
  public void addIndicator(String name, String unit, double mean, double median, double std) {
    indicators.put(Indicator.formatName(name),new Indicator(name,unit,mean,median,std));
  }
  
  /** 
   * Remove any indicator with the given name
   * 
   * @param name String name of indicator to remove
   */
  public void removeIndicator(String name) {
    indicators.remove(Indicator.formatName(name));
  }
  
  /**
   * Get the indicator with the given name
   * 
   * @param name String name of indicator to retrieve
   * 
   * @return Indicator on success, NULL on failure
   */
  public Indicator getIndicator(String name) {
    return indicators.get(Indicator.formatName(name));
  }
  
  /**
   * Get all indicator names
   * 
   * @return Array of string indicator names stored in alternative
   */  
  public String[] getIndicatorNames() {
    return indicators.keySet().toArray(new String[0]);
  }
  
  /**
   * Get name of alternative
   * 
   * @return String name of alternative
   */
  public String getName()
  {
    return this.name;
  }
  
  /**
   * <code>formatName</code> formats name into format of indicator names.
   * 
   * @param name String potentially naming an indicator
   *
   * @return String name formatted as an indicator name
   * 
   * NOTE: This method is static, and therefore does not effect actual indicator
   **/
  static public String formatName(String name) {
    return WordUtils.capitalize(name).replace(" ", "");
  }
  
}
