/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca.smaa;

import java.util.Arrays;
import org.apache.commons.math3.distribution.*;

/**
 * Employs the SMAA-TY algorithm for weighting indicators with beta 
 * distributions
 * 3/2/2014 (nagarjuna): changed constructor to optimize memory usage
 * 4/26/2014 (mison): added functionality for priorities
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Feb 23, 2014
 */
public class SMAA_TY {
  
  private int numWeights;
  private int numRuns;
  private double[][] weights; // Calculated weights, stored for later use
  
  private int numPriorities; // number of priorities to use
  private double[] priorityTable; // Table of priority weighting
  private double[] priorities; // Priority for each weight
  
  private BetaDistribution[] betaDist; // Beta Distributions for each weight
  
  /**
   * Generate a SMAA-TY weight distribution set for given number of weights
   * 
   * @param numW number of weights in each set
   */
  public SMAA_TY(int numW) {
    /**
     * Changed by Nagarjuna
     * save memory by just initiating distribution for given number of weights
     * and get sample when required.
     */
    init(numW, 0, 1);
  }
  
  /**
   * Generate a SMAA_TY weight set for a given number of weights and sets
   * @param numW number of weights in each set
   * @param numR number of random sample sets to generate
   */
  public SMAA_TY(int numW, int numR) {
    init(numW, numR, 1);
  }
  
  /**
   * Generate a SMAA_TY weight set for given number of weights and sets with potential priorities
   * @param numW number of weights in each set
   * @param numR number of random sample sets to generate
   * @param numP number of choices for priorities
   */
  public SMAA_TY(int numW, int numR, int numP) {
    if (numP < 1)
      numP = 1;
    
    init(numW, numR, numP);
  }
  
  /**
   * 
   * @param numP new number of priorities
   */
  public void setNumPriorities(int numP) {
    if (numP < 1 || numP == numPriorities)
      return;
    
    if (numP % 2 == 0)
      numP += 1;
    
    numPriorities = numP;
    
    // Reset weights
    resetWeights();
  }
  
  public int getNumPriorities() {
    return numPriorities;
  }
  
  public int getNumWeights() {
    return numWeights;
  }
  
  public int getNumSamples() {
    return numRuns;
  }
  
  /**
   * Set individual priority 
   * @param weightInd index of priority
   * @param p new priority (1 to numPriorities)
   */
  public void setPriority(int weightInd, int p) {
    if (weightInd < 0 || weightInd >= numWeights || p < 1 || p >= numPriorities || priorities[weightInd] == priorityTable[p-1])
      return;
    
    priorities[weightInd] = priorityTable[p-1];
    
    resetWeights();
  }
  
  /**
   * Set all priorities at once
   * 
   * @param p array of integer priorities (1 to numPriorities)
   */
  public boolean setPriorities(int[] p) {
    
    if (p.length != priorities.length)
      return false;
    
    boolean resetW = false;
    for (int i = 0; i < priorities.length; i++) {
      if (p[i] > 0 && p[i] <= numPriorities && priorities[i] != priorityTable[p[i]-1]) {
        priorities[i] = priorityTable[p[i]-1];
        resetW = true;
      }
    }
    
    // Reset weights only if value has changed
    if (resetW) {
      resetWeights();
    }
      
    return resetW;
  }
  
  /**
   * Set an array of weights for a given number of samples
   * 
   * @param numW number of weights to use
   * @param numR number of sample sets to generate
   * 
   */
  public void setWeights(int numW, int numR) {
    // Check if new calculation needed
    if (numW < 0 || numR < 0 || 
        (numR == this.weights.length && this.weights.length > 0 && numW == this.weights[0].length))
      return;
    
    this.numWeights = numW;
    this.numRuns = numR;
    
    resetWeights();
  }
  
  public double[][] getNormalizedWeights() {
    double[][] normWeight = new double[this.weights.length][this.weights[0].length];
    
    double minAltValue = 0.0;
    double maxAltValue = 100.0;
    for (int i = 0; i < weights.length; i++) {
      for (int j = 0; j < weights[i].length; j++) {
        normWeight[i][j] = (weights[i][j] - minAltValue) / (maxAltValue-minAltValue);
        // Problem with rounding due to double not able to represent -100/100 perfectly, maps to -7E-17 or 1+1E-14.
        if (normWeight[i][j] < 0 && normWeight[i][j] > -1E-14) {            
          //System.out.println("Problem with rounding: "+ flow[j]);
          normWeight[i][j] = 0.;
        } else if (normWeight[i][j] > 1 && normWeight[i][j] < 1+1E-14) {
          //System.out.println("Problem with rounding: "+ flow[j]);
          normWeight[i][j] = 1.;
        }
      }
    }
    return normWeight;
  }
    
  public double[] getWeights(int sampInd) {
    if (sampInd < 0 || sampInd >= this.weights.length)
      sampInd = 0;
    
    return this.weights[sampInd];
  }
  
  /**
   * Generate a single set of SMAA-TY weights
   * 
   * @return double array of weights, summing to 100
   */
  public double[] sampleWeights() {
    double[] minWeights = setMinWeights();
    return sampleWeights(minWeights);
  }
  
  /**
   * Generate a single set of SMAA-TY weights with priorities
   * 
   * @return double array of weights, summing to 100
   * 
   * @param minWeights minimum weights according to priorities
   * @return 
   */
  private double[] sampleWeights(double[] minWeights) {
    if (numWeights <= 0)
      return new double[0];
    
    // Sum all minimum weights for initial scaling
    double minSum = 0;
    for (int i = 0; i < minWeights.length; i++)
      minSum+=minWeights[i];
    
    double[] w = new double[this.numWeights];
    double scale = 100-minSum;
    for (int n = numWeights-1; n > 0; n--) {
          // Select from beta distribution	
          w[n] = this.betaDist[n].sample()*scale;
          scale -= w[n]; // Decrement scale
          w[n] += minWeights[n]; // Add minimum for priorities
    }
    w[0] = scale+minWeights[0];
    
    return w;
  }
  
  /**
   * Reset the weights for the given number of weights and samples
   */
  private void resetWeights() {
    this.weights = new double[numRuns][numWeights];
    
    if (numWeights != this.priorities.length)
      resetPriorities();
    
    // Initialize beta distributions
    initDists();
    
    double[] minWeights = setMinWeights();
    //Go through each run
    for (int i = 0; i < numRuns; i++) {
      // Initialize scale parameter for given run
      double weightSample[] = sampleWeights(minWeights);
      System.arraycopy(weightSample, 0, this.weights[i], 0, weightSample.length);
    }
  }
  
  /**
   * Initialize all parameters and set weights
   * 
   * @param numW number of weights to use
   * @param numR number of samples to take
   * @param numP number of priority choices to use
   */
  private void init(int numW, int numR, int numP) {
    numWeights = 0;
    numRuns = 0;
    numPriorities = 1;
    if (numW > 0)
      numWeights = numW;
    if (numR > 0)
      numRuns = numR;
    if (numP > 1)
      numPriorities = numP;
    
    // Priorities should be odd
    if (numPriorities % 2 == 0)
      numPriorities++;
    
    this.weights = new double[0][0];
    this.priorities = new double[0];
    resetWeights();
  }
  
  /**
   * Set priorities to default value with proper length
   */
  private void resetPriorities() {
    this.priorities = new double[numWeights];
    setPriorityTable();
    double defP = defaultPriority();
    for(int i = 0; i < this.priorities.length; i++)
      this.priorities[i] = defP;
    
  }
  
  private void setPriorityTable() {
    this.priorityTable = new double[numPriorities];
    double def = 100/(double)numWeights;
    double step = def*.25;
    double offset = 0;
    int startInd = numPriorities/2;
    for (int i = startInd; i < priorityTable.length; i++) {
      priorityTable[i] = def + offset;
      priorityTable[2*startInd-i] = def - offset;
      offset+=step;
    }
  }
  
  private double[] setMinWeights() {
    double sum = 0;
    double minVal = 100;
    for (int i = 0; i < this.priorities.length; i++) {
      sum += priorities[i];
      if (priorities[i] < minVal)
        minVal = priorities[i];
    }
    double[] minWeights = new double[this.priorities.length];
    minVal = minVal/sum;
    for (int i = 0; i < this.priorities.length; i++) {
      minWeights[i] = (priorities[i]/sum - minVal)*100;
      
    }
    //System.out.println(Arrays.toString(minWeights));
    return minWeights;
  }
  
  /**
   * 
   * @return default priority half of range
   */
  private double defaultPriority() { 
    return this.priorityTable[numPriorities/2];
  }
  
  /**
   * Initialize beta distributions for a given number of weights
   */
  private void initDists() {
    this.betaDist = new BetaDistribution[numWeights];
    double beta = numWeights-1;
    double alpha = 1;
    for (int i = numWeights-1; i >= 0; i--) {
      this.betaDist[i] = new BetaDistribution(alpha, beta--);
    }
    
  }
  
  /**
   * Method for getting the sample of current distribution when required
   * @return returns weights generated for set criteria count
   */
  public double[] getNextSample() {
      return sampleWeights();
  }
}
