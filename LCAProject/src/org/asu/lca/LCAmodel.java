/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca;

import org.asu.lca.sharedobject.SharedDataObject;
import org.asu.lca.smaa.Alternative;
import org.asu.lca.smaa.Comparison;
import org.asu.lca.smaa.Indicator;

/**
 * This is the model holding state information for interaction
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Apr 6, 2014
 */
public class LCAmodel {
  public static final int INDICATORS = 1;
  public static final int MEAN_RANK = 2;
  public static final int MIN_RANK = 3;
  public static final int ALT_RANK = 4;
  public static final int COMPARE = 5;
  
  public static final int PDF = 1;
  public static final int CDF = 2;
  
  public static final String[] PRIORITY_NAMES = {"Low", "", "Average", "", "High"};
  
  private Comparison compare;
  
  public LCAmodel() {
    init();
    
  }
  
  public void loadData(SharedDataObject sdo) {
    init();
    setAlternatives(sdo.getAlternatives());
    // If alternatives not null, preprocess columns and data
    setIndicators(sdo);
  }
  
  public boolean computeRankings() {
    if (compare.getAlternativeNames().length == 0)
      return false;
    
    compare.setRankings();
    return true;
  }
  
  public void updateIndicators(int numInds, int type) {
    compare.selectIndicators(numInds,type);
  }
  
  public boolean updatePriorities(int[] priorities) {
      return compare.setIndicatorPriorities(priorities);
  }
  
  public void runAnalysis(int nRuns) {
    compare.compareAlternatives(nRuns);
  }
  /**
   * get data read from excel file
   * @return 
   */
  public Object[][] getTableData() {
    return getTableData(INDICATORS);
  }
  
  public Object[][] getTableData(int type) {
    // Fixed by mison after bug found from Nagarjuna when loading only areas
    switch(type) {
      case MEAN_RANK:
        return compare.getMeanRanking();
      case MIN_RANK:
        return compare.getMinRanking();
      case ALT_RANK:
        return compare.getAltRanking();
      default:
        return compare.getData();
    }
  }
  
  public double[][] getContributionData(String altName, String indName, int plotType) {
    String[] aNames = generateAltList(altName);
    
    String[] iNames = generateIndList(indName);

    return compare.getIndividualDists(aNames, iNames, plotType);
  }
  
  public String[] getContributionLabels(String altName, String indName) {
    String[] aNames = generateAltList(altName);
    
    String[] iNames = generateIndList(indName);

    return compare.getIndividualDistNames(aNames, iNames);
  }
  
  public double[][] getComparisonData(int plotType) {
    return compare.getComparisonDists(plotType);
  }
  
  public double[][] getRankPlotData() {
    return compare.getAltHistData();
  }
  
  public double[][] getWeightData(int plotType) {
    return compare.getWeightDists(plotType);
  }
  
/**
   * get column types read from excel file
   * @return 
   */
  public String[] getColumnTypes() {
      return getColumnTypes(INDICATORS);
  }

  public String[] getColumnTypes(int type) {
    switch(type) {
      case MEAN_RANK:
        return new String[]{"Rank","Indicator","Mean Area"};
      case MIN_RANK:
        return new String[]{"Rank","Indicator","Min Area"};
      case ALT_RANK:
        int len = compare.getAlternativeNames().length;
        String[] colTypes = new String[len+1];
        colTypes[0] = "Alt\\Rank";
        for (int i = 1; i < colTypes.length; i++)
          colTypes[i] = String.valueOf(i);
        return colTypes;
      default:
        return compare.getColumnTypes();
    }
  }
  
  public String[] getAlternatives() {
    return compare.getAlternativeNames();
  }
  
  public String[] getIndicators() {
    return compare.getSelectedIndicators();
  }
  
  private void init() {
    compare = new Comparison(1,PRIORITY_NAMES.length);
  }
  
  private void setAlternatives(Object[] alternatives) {
    if(alternatives == null || alternatives.length == 0) {
        return;
    }
    for (Object alternative : alternatives) {
      compare.addAlternative(alternative.toString());
    }
  }
  
      // Modified to include units
  private void setIndicators(SharedDataObject sdo) {
    Object[] alternatives = sdo.getAlternatives();
    String[] colNames = sdo.getColumnTypes();
    Object[][] dat = sdo.getData();
    for (Object[] dat1 : dat) {
      String indName = Indicator.formatName(dat1[0].toString());
      String unit = "";
      double mean = -1;
      double median = -1;
      double sd = -1;
      int altInd = 0;
      for (int j = 1; j < colNames.length; j++) {
        String d = dat1[j].toString();
        String col = colNames[j];
        if(col.matches("(?i:(.*)(unit|medida)(.*))"))
          unit = d;
        else if(col.matches("(?i:(.*)(mean|mu)(.*))"))
          mean = Double.parseDouble(d);
        else if(col.matches("(?i:(.*)(median|med)(.*))"))
          median = Double.parseDouble(d);
        else if(col.matches("(?i:(.*)(sd|sigma)(.*))"))
          sd = Double.parseDouble(d);
        if (mean>-1 && median > -1 && sd > -1){
          compare.getAlternative(Alternative.formatName(alternatives[altInd++].toString()))
                  .addIndicator(indName, unit, mean, median, sd); 
          mean = -1; median = -1; sd = -1;
        }
      }
    }
  }
  
  private String[] generateIndList(String indName) {
    if (indName.equals("All"))
      return compare.getSelectedIndicators();
    else
      return new String[] {indName};
  }
  
  private String[] generateAltList(String altName) {
    if (altName.equals("All"))
      return compare.getAlternativeNames();
    else
      return new String[] {altName};
  }
  
}
