/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca.sharedobject;

import org.asu.lca.smaa.Alternative;
//import org.asu.lca.smaa.Comparison;
import org.asu.lca.smaa.Indicator;
import java.util.ArrayList;

/**
 * @desc Data Object shared between Excel reading and
 * table populating.
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi (nthottem@asu.edu)
 * @version Feb 22, 2014
 * 
 * Updated Feb 16 by mison to include ranking comparisons
 * Updated Feb 22 by mison to include units in data
 */
public class SharedDataObject {
  public static final int INDICATORS = 1;
  public static final int MEAN_RANK = 2;
  public static final int MIN_RANK = 3;
  public static final int ALT_DISTR = 4;
  
    Object[] alternatives;
    String[] columnType;
    Object[][] data;
    //Comparison c;
    //public SharedDataObject() {
    //    c = new Comparison();
    //}
    
        /**
     * Constructor with default organization, no alternatives
     * 
     * @param colNames Column names for data to organize
     * @param dat Indicator names and data for each alternative
     */
    public SharedDataObject(ArrayList<String> colNames, 
                            ArrayList<ArrayList<String>> dat) {
      //c = Comparison.getInstance();
      //c.init(1000);
      setAlternatives(null);

      setColumnTypes(colNames);
      setData(dat);  
    }
    
    /**
     * Constructor with default organization
     * 
     * @param altNames Alternative names in correct order
     * @param colNames Column names for data to organize
     * @param dat Indicator names and data for each alternative
     */
    public SharedDataObject(ArrayList<String> altNames, 
                            ArrayList<String> colNames, 
                            ArrayList<ArrayList<String>> dat) {
      //c = Comparison.getInstance();
      //c.init(1000);
      setAlternatives(altNames);
      
     
      // If alternatives not null, preprocess columns and data
      /*if (alternatives != null)
      {
        // Format data corresponding to indicators
        setIndicators(colNames, dat);
        resetInputData(colNames, dat);
        c.setRankings(); // Prevent duplicate calls to setRankings
      }*/
      
      setColumnTypes(colNames);
      setData(dat);  
    }
    /**
     * Set alternatives read from excel file
     * @param alternatives 
     */
    private void setAlternatives(ArrayList<String> alternatives) {
        if(alternatives == null) {
            this.alternatives = null;
            return;
        }
        this.alternatives = alternatives.toArray();
        for (int i = 0; i < this.alternatives.length; i++)
        {
          this.alternatives[i] = Alternative.formatName(this.alternatives[i].toString());
          //Alternative test = c.addAlternative(this.alternatives[i].toString());
          //this.alternatives[i] = test.getName();
        }
    }
    
    /**
     * set column type read from excel file
     * @param columnType 
     */
    private void setColumnTypes(ArrayList<String> columnType) {
        if(columnType == null) {
            this.columnType = null;
            return;
        }
        int i = 0;
        this.columnType = new String[columnType.size()];
        for(String s: columnType) {
            this.columnType[i++] = s;
        }
    }
    /**
     * set data read from excel file
     * @param data 
     */
    private void setData(ArrayList<ArrayList<String>> data) {
        if(data == null) {
            this.data = null;
            return;
        }
        int rows = data.size();
        int cols = data.get(0).size();
        this.data = new Object[rows][cols];
        for(int i = 0 ; i < rows; i++) {
            this.data[i] = data.get(i).toArray();
            this.data[i][0] = Indicator.formatName(this.data[i][0].toString());
        }
    }
    
    /**
     * get alternative read from excel file
     * @return 
     */
    public Object[] getAlternatives() {
        return alternatives;
    }
    
    /**
     * get column types read from excel file
     * @return 
     */
    public String[] getColumnTypes() {
        return getColumnTypes(INDICATORS);
    }
    
    public String[] getColumnTypes(int type) {
      // Fixed by mison after bug found from Nagarjuna when loading only areas
      //if (this.alternatives == null) 
        return columnType;
      
      //switch(type) {
        //case MEAN_RANK:
        //  return new String[]{"Ranking","Indicator","Mean Area"};
        //case MIN_RANK:
        //  return new String[]{"Ranking","Indicator","Min Area"};
        //default:
        //  return columnType;
      //}
    }
    
    /**
     * get data read from excel file
     * @return 
     */
    public Object[][] getData() {
        return getData(INDICATORS);
    }
    
    public Object[][] getData(int type) {
      // Fixed by mison after bug found from Nagarjuna when loading only areas
      //if (this.alternatives == null) 
        return data;
      
      /*switch(type) {
        case MEAN_RANK:
          return c.getMeanRanking();
        case MIN_RANK:
          return c.getMinRanking();
        default:
          return data;
      }*/
    }
    
    // Modified to include units
    private void setIndicators(ArrayList<String> colNames, ArrayList<ArrayList<String>> dat) {
      for (int i = 0; i < dat.size(); i++) {
        String indName = Indicator.formatName(dat.get(i).get(0));
        String unit = "";
        double mean = -1;
        double median = -1;
        double sd = -1;
        int altInd = 0;
        for (int j = 1; j < colNames.size(); j++) {
          if(colNames.get(j).matches("(?i:(.*)(unit|medida)(.*))"))
            unit = dat.get(i).get(j);
          else if(colNames.get(j).matches("(?i:(.*)(mean|mu)(.*))"))
            mean = Double.parseDouble(dat.get(i).get(j));
          else if(colNames.get(j).matches("(?i:(.*)(median|med)(.*))"))
            median = Double.parseDouble(dat.get(i).get(j));
          else if(colNames.get(j).matches("(?i:(.*)(sd|sigma)(.*))"))
            sd = Double.parseDouble(dat.get(i).get(j));
          
          if (mean>-1 && median > -1 && sd > -1){
            //c.getAlternative(this.alternatives[altInd++].toString())
            //   .addIndicator(indName, unit, mean, median, sd); 
                                      
            mean = -1; median = -1; sd = -1;
          }
        }
      }
    }
    
    private void resetInputData(ArrayList<String> colNames, ArrayList<ArrayList<String>> dat) {
      //this.alternatives = c.getAlternativeNames();
      //String[] indicators = c.getCommonIndicators();
      dat.clear();
      colNames.clear();
      /*
      for (int i = 0; i < indicators.length; i++)
      {
        if (i == 0) {
          colNames.add("Indicator");
          colNames.add("Units");
        }
        dat.add(new ArrayList<String>());
        dat.get(i).add(indicators[i]);
        // Include units per V request
        dat.get(i).add(c.getAlternative(this.alternatives[0].toString()).getIndicator(indicators[i]).getUnits());
        for (Object alternative : this.alternatives) {
          if (i == 0) {
            colNames.add(alternative.toString() + ":Mean");
            colNames.add(alternative.toString() + ":SD");
          }
          //do not format. Smaller values are presented as zeroes.  
          // Small values still showing up as 0 with Rogers dataset, Scientific notation?
          //dat.get(i).add(String.format("%f",c.getAlternative(alternative.toString()).getIndicator(indicators[i]).getMean()));
          //dat.get(i).add(String.format("%f",c.getAlternative(alternative.toString()).getIndicator(indicators[i]).getSD()));
          dat.get(i).add(String.format("%.3e",c.getAlternative(alternative.toString()).getIndicator(indicators[i]).getMean()));
          dat.get(i).add(String.format("%.3e",c.getAlternative(alternative.toString()).getIndicator(indicators[i]).getSD()));
        }
        
      }*/
    }
}
