/* LCA Impact Comparison Analysis for Valentina Prado-Lopez:
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation.
 *
 * This class is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.asu.lca;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableModel;
import org.asu.lca.excelreader.ExcelReadWriter;
import org.asu.lca.excelreader.IncorrectFileFormatException;
import org.asu.lca.excelreader.XLSReadWriter;
import org.asu.lca.excelreader.XLSXReadWriter;
import org.asu.lca.sharedobject.SharedDataObject;

/**
 * This is the controller providing functionality between model and view
 * 
 * @author Mark Ison (mison@asu.edu)
 * @author Nagarjuna Thottempudi
 * @version Apr 6, 2014
 */
public class LCAcontroller {
  private static final int HEADER_TYPE_LCA = 2;
  
  private static final int MEAN_AREA_TABLE = LCAmodel.MEAN_RANK;
  private static final int MIN_AREA_TABLE = LCAmodel.MIN_RANK;
  private static final int ALT_RANK_TABLE = LCAmodel.ALT_RANK;
  
  private final LCAmodel theModel;
  private final LCAview theView;
  private String loadFileName;
  
  public LCAcontroller(LCAview view, LCAmodel model) {
    this.theView = view;
    this.theModel = model;
    this.loadFileName = null;
    
    // Add action listeners!
    addActionListeners();
  }
  
  private void addActionListeners() {
    // Menu Items
    addInputFileFormatListener();
    addAboutListener();
    
    // Panel GUI
    addLoadDataListener();
    addSaveRankListener();
    addRunListener();
    addIndSelectListener();
    addContListener();
    addCompareListener();
    addRankPlotListener();
    addWeightPlotListener();
  }
  
  private void addLoadDataListener() {
    ActionListener loadListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        loadData();
      }
    };
    theView.addLoadDataListener(loadListener);
  }
  
    
  private void addSaveRankListener() {
    ActionListener saveListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        saveRank();
      }
    };
    theView.addSaveRankListener(saveListener);
  }
  
  private void addRunListener() {
    ActionListener runListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        runAnalysis();
      }
    };
    theView.addRunListener(runListener);
  }
  
  private void addIndSelectListener() {
    ActionListener selectListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        selectIndicators();
      }
    };
    ChangeListener changeListener = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent changeEvent) {
        selectIndicators();
      }
    };
    theView.addIndSelectListener(selectListener, changeListener);
  }
  
  private void addContListener() {
    ActionListener contListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        //first plot the image then open window. Otherwise popup window
        //closes because of p.waitFor() in image type.
        contributionAnalysis(false);  
        contributionAnalysis(true);
      }
    };
    theView.addContributionListener(contListener);
  }
  
  private void addCompareListener() {
    ActionListener compListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        comparisonResults(true);
      }
    };
    theView.addComparisonListener(compListener);
  }
  
    
  private void addWeightPlotListener() {
    ActionListener wListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        weightDistribution(true);
      }
    };
    theView.addWeightPlotListener(wListener);
  }
  
  private void addRankPlotListener() {
    ActionListener compListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        rankResults();
      }
    };
    theView.addRankPlotListener(compListener);
  }
  
  private void addInputFileFormatListener() {
    ActionListener formatListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        openExampleFile();
      }
    };
    theView.addInputFileFormatListener(formatListener);
  }
  
  private void addAboutListener() {
    ActionListener aboutListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        theView.showAboutFrame();
      }
    };
    theView.addAboutListener(aboutListener);
  }
  
  private void loadData() {
    File theFile = theView.selectDataFile();
    if (theFile != null) {
      this.loadFileName = theFile.getAbsolutePath();
      SharedDataObject sdo = readDataFile(theFile, HEADER_TYPE_LCA);
      theModel.loadData(sdo);
      theView.setDataTable(theModel.getColumnTypes(), theModel.getTableData());
      if (theModel.computeRankings()) {
        theView.setRankingTable(MEAN_AREA_TABLE, theModel.getColumnTypes(MEAN_AREA_TABLE), theModel.getTableData(MEAN_AREA_TABLE));
        theView.setRankingTable(MIN_AREA_TABLE, theModel.getColumnTypes(MIN_AREA_TABLE), theModel.getTableData(MIN_AREA_TABLE));
        
        theView.showSettings();
        selectIndicators();
        theView.hideResults();
      }
    }
  }
  
  private void saveRank() {
    String saveName = "";
    if (this.loadFileName !=null)
      saveName = this.loadFileName.replace(".", "_ranking.");
    File theFile = theView.selectSaveRankFile(saveName);
    if (theFile != null) {
      ExcelReadWriter er;
      if (theFile.getAbsolutePath().endsWith(".xlsx")) {
        er = new XLSXReadWriter();
      }
      else {
        er = new XLSReadWriter();
      }
      ArrayList<TableModel> tModels = new ArrayList<>();
      // Place both rankings on same sheet for easier comparison
      tModels.add(new MyTableModel(theView.getCombinedRankData(), theView.getCombinedRankCols()));
      // Include original data
      tModels.add(new MyTableModel(theModel.getTableData(), theModel.getColumnTypes()));
      er.writeToFile(theFile,tModels);
      theView.showMessage(theFile.getName() + " saved!", "File Save Message", JOptionPane.INFORMATION_MESSAGE);
    }
  }
  
  private void selectIndicators() {
    theModel.updateIndicators(theView.getNumIndicators(),
                            theView.getAreaType());
    theView.setContributionOptions(theModel.getAlternatives(), theModel.getIndicators());
    
    ChangeListener changeListener = new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent changeEvent) {
        updatePriorities();
      }
    };
    
    theView.updatePriorities(theModel.getIndicators(), changeListener);
  }
  
  private void updatePriorities() {
    if (theModel.updatePriorities(theView.getPriorities()))
      updateAnalysis();
  }
  
  private void runAnalysis() {
    theView.setContributionOptions(theModel.getAlternatives(), theModel.getIndicators());
    updateAnalysis();
  }
  
  private void updateAnalysis() {
    theModel.runAnalysis(theView.getNumRuns());
    theView.setAltRankTable(theModel.getColumnTypes(ALT_RANK_TABLE), theModel.getTableData(ALT_RANK_TABLE));
    theView.updateResults();
    comparisonResults(false);
    weightDistribution(false);
    contributionAnalysis(false);
  }
  
  private void comparisonResults(boolean newWindow) {
    int type = theView.getDistType();
    theView.plotComparison(theModel.getComparisonData(type), theModel.getAlternatives(), newWindow);
  }
  
  private void weightDistribution(boolean newWindow) {
    int type = theView.getDistType();
    theView.plotWeights(theModel.getWeightData(type), theModel.getIndicators(), newWindow);
  }
  
  
  private void rankResults() {
    theView.plotAltRankings(theModel.getRankPlotData(), theModel.getAlternatives());
  }
  
  private void contributionAnalysis(boolean newWindow) {
    String alt = theView.getAltContribution();
    String ind = theView.getIndContribution();
    int type = theView.getDistType();
    theView.plotContribution(
      theModel.getContributionData(alt,ind,type),
      theModel.getContributionLabels(alt,ind), newWindow);
  }
  
  private void openExampleFile() {
    if(isWindows()) {
      try { 
        Process p = Runtime.getRuntime().exec(new String[]{"cmd.exe","/C","dataEntryExample.xlsx"});
      }
      catch (IOException ex) {
        Logger.getLogger(LCA.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    else if(isUnix()) {
      String software = "soffice";
      try {
        Process p = Runtime.getRuntime().exec(software+" dataEntryExample.xlsx");
      }
      catch (IOException ex) {
        if (ex.getMessage().contains("error=2, No such file or directory")) {
          theView.showMessage("Open Office not found. Please install and try again.", "Software Not Detected", JOptionPane.INFORMATION_MESSAGE);
        }
      }
    }
    else { 
      theView.showMessage("This OS not currently supported", "OS Not Supported", JOptionPane.INFORMATION_MESSAGE);
    }
  }
  
  private SharedDataObject readDataFile(File file, int headerType) {
    FileInputStream f;
    SharedDataObject sdo=null;
    try { 
      f = new FileInputStream(file.getAbsolutePath());
      ExcelReadWriter er;
      if (file.getAbsolutePath().endsWith(".xlsx")) {
        er = new XLSXReadWriter();
      }
      else {
        er = new XLSReadWriter();
      }
      if (headerType == HEADER_TYPE_LCA)
        sdo = er.processLCAFile(f);
      else
        sdo = er.processFile(f);
      if (sdo == null)
        throw new IncorrectFileFormatException("Data Processing Error");
    }
    catch (FileNotFoundException ex) {
      theView.showMessage(file.getName()+ " not found", "File not found", JOptionPane.ERROR_MESSAGE);
    }
    catch (IncorrectFileFormatException ex) {
      theView.showMessage("Error: " + ex.getMessage(), "Incorrect File Format", JOptionPane.ERROR_MESSAGE);
    }
    return sdo;
  }
  
  public static boolean isWindows() { 
    return (System.getProperty("os.name").toLowerCase().indexOf("win")>= 0);
  }
  
  public static boolean isMac() { 
    return (System.getProperty("os.name").toLowerCase().indexOf("mac")>= 0);
  }
  
  public static boolean isUnix() { 
    String OS = System.getProperty("os.name").toLowerCase();
    return (OS.indexOf("nix")>= 0 || OS.indexOf("nux") >=0 || OS.indexOf("aix")>=0);
  }
  
  public static boolean isSolaris() { 
    return (System.getProperty("os.name").toLowerCase().indexOf("sunos")>= 0);
  }
  
}
