import java.awt.Color;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *  scatter plot.
 */
public class ScatterPlot extends ApplicationFrame {

	private static final long serialVersionUID = -6172379856426539187L;
    private final XYSeriesCollection data = new XYSeriesCollection();
    private String sName = null;
	/**
     * application showing a scatter plot.
     *
     * @param title  the frame title.
   * @param dataType
   * @param values
     */
    public ScatterPlot(String title, String dataType, double[] values) {
        super(title);
        sName = title;
        setData(dataType, values);
        JPanel chartPanel = createDemoPanel();
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        setContentPane(chartPanel);
    }

    private JFreeChart createChart(XYDataset dataset) {
        JFreeChart chart = ChartFactory.createScatterPlot(sName,
                "X", "Y", dataset, PlotOrientation.VERTICAL, true, false, false);
 
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setNoDataMessage("NO DATA");
        plot.setDomainZeroBaselineVisible(true);
        plot.setRangeZeroBaselineVisible(true);
       
        XYLineAndShapeRenderer renderer
                = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setSeriesOutlinePaint(0, Color.black);
        renderer.setUseOutlinePaint(true);
        NumberAxis domainAxis = (NumberAxis) plot.getDomainAxis();
        domainAxis.setAutoRangeIncludesZero(false);
        domainAxis.setTickMarkInsideLength(2.0f);
        domainAxis.setTickMarkOutsideLength(0.0f);
       
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setTickMarkInsideLength(2.0f);
        rangeAxis.setTickMarkOutsideLength(0.0f);
        
        renderer.setSeriesLinesVisible(0, true);
        plot.setRenderer(renderer);
       
        return chart;
    }
   
    /**
     * Creates a panel for the demo 
     *
     * @return A panel.
     */
    public final JPanel createDemoPanel() {           
        JFreeChart chart = createChart(data);
        ChartPanel chartPanel = new ChartPanel(chart);
        //chartPanel.setVerticalAxisTrace(true);
        //chartPanel.setHorizontalAxisTrace(true);
        // popup menu conflicts with axis trace
        chartPanel.setPopupMenu(null);
       
        chartPanel.setDomainZoomable(true);
        chartPanel.setRangeZoomable(true);
        return chartPanel;
    }
    
    private void setData(String datatype, double[] values) {
    	final XYSeries series = new XYSeries(datatype, false);
    	for(int i = 0; i < values.length; i++)
    		series.add(i, values[i]);
    	data.addSeries(series);
    }
   
    /**
     * Starting point for the scatter plot application.
     *
     * @param args  ignored.
     */
    public static void main(String[] args) {
    	double[] data = {1,0.2,0.3,2,3,4,7};
    	ScatterPlot demo = new ScatterPlot("Scatter Plot", "Random points", data);
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

}
